from __future__ import division

import math
import unittest

import sample_data
from assets.model.density_dependent_survival import DensityDependentSurvival


class TestDensityDependentSurvival(unittest.TestCase):

    def test_kill_within_field_nongendered(self):
        field = DensityDependentSurvival(False, False, True).__kill_within_field__(sample_data.bt_field_instance1)
        self.assertTrue(math.floor((100 * 50) / (160 * 2)) <= field['subpop']['rr_m'] <= math.ceil((100 * 50) / (160 * 2)))
        self.assertTrue(math.floor((100 * 50) / (160 * 2)) <= field['subpop']['rr_f'] <= math.ceil((100 * 50) / (160 * 2)))
        self.assertTrue(math.floor((100 * 60) / (160 * 2)) <= field['subpop']['rs_m'] <= math.ceil((100 * 60) / (160 * 2)))
        self.assertTrue(math.floor((100 * 60) / (160 * 2)) <= field['subpop']['rs_f'] <= math.ceil((100 * 60) / (160 * 2)))
        self.assertTrue(math.floor((100 * 50) / (160 * 2)) <= field['subpop']['ss_m'] <= math.ceil((100 * 50) / (160 * 2)))
        self.assertTrue(math.floor((100 * 50) / (160 * 2)) <= field['subpop']['ss_f'] <= math.ceil((100 * 50) / (160 * 2)))

    def test_kill_within_field_gendered(self):
        field = DensityDependentSurvival(True, False, True).__kill_within_field__(sample_data.bt_field_instance1)
        self.assertEqual((100 * 20) // 160, field['subpop']['rr_m'])
        self.assertEqual((100 * 30) // 160, field['subpop']['rr_f'])
        self.assertEqual((100 * 25) // 160, field['subpop']['rs_m'])
        self.assertEqual((100 * 35) // 160, field['subpop']['rs_f'])
        self.assertEqual((100 * 25) // 160, field['subpop']['ss_m'])
        self.assertEqual((100 * 25) // 160, field['subpop']['ss_f'])

    def test_kill_within_field_deterministic(self):
        field = DensityDependentSurvival(False, False, False).__kill_within_field__(sample_data.bt_field_instance1)
        self.assertEqual((100 * 20) / 160, field['subpop']['rr_m'])
        self.assertEqual((100 * 30) / 160, field['subpop']['rr_f'])
        self.assertEqual((100 * 25) / 160, field['subpop']['rs_m'])
        self.assertEqual((100 * 35) / 160, field['subpop']['rs_f'])
        self.assertEqual((100 * 25) / 160, field['subpop']['ss_m'])
        self.assertEqual((100 * 25) / 160, field['subpop']['ss_f'])

if __name__ == '__main__':
    unittest.main()