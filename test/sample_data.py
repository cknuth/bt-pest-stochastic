two_fields1 = [
            [
                {
                    "bt": True,
                    "capacity": 10000,
                    "subpop": {
                        "rr_m": 40,
                        "rr_f": 30,
                        "rs_m": 980,
                        "rs_f": 980,
                        "ss_m": 3980,
                        "ss_f": 3990
                    }
                },
                {
                    "bt": False,
                    "capacity": 1000,
                    "subpop": {
                        "rr_m": 0,
                        "rr_f": 0,
                        "rs_m": 99,
                        "rs_f": 98,
                        "ss_m": 402,
                        "ss_f": 401
                    }
                }
            ]
        ]

two_fields2 = [
            [
                {
                    "bt": True,
                    "capacity": 10000,
                    "subpop": {
                        "rr_m": 0,
                        "rr_f": 0,
                        "rs_m": 1020,
                        "rs_f": 1010,
                        "ss_m": 3980,
                        "ss_f": 3990
                    }
                }
            ],
            [
                {
                    "bt": False,
                    "capacity": 1000,
                    "subpop": {
                        "rr_m": 0,
                        "rr_f": 0,
                        "rs_m": 99,
                        "rs_f": 98,
                        "ss_m": 402,
                        "ss_f": 401
                    }
                }
            ]
        ]


bt_field_instance1 = {
    "bt": True,
    "capacity": 100,
    # total: 160
    "subpop": {
        "rr_m": 20,
        "rr_f": 30,
        "rs_m": 25,
        "rs_f": 35,
        "ss_m": 25,
        "ss_f": 25
    }
}


bt_field_instance2 = {
            "bt": True,
            "capacity": 10000,
            "subpop": {
                "rr_m": 20,
                "rr_f": 40,
                "rs_m": 980,
                "rs_f": 990,
                "ss_m": 3880,
                "ss_f": 3990
            }
        }

subpop1 = {
    "rr_m": 0,
    "rr_f": 0,
    "rs_m": 99,
    "rs_f": 98,
    "ss_m": 402,
    "ss_f": 401
}

# total: 700
field_instance1 = {
    "bt": True,
    "capacity": 10000,
    "subpop": {
        "rr_m": 20,
        "rr_f": 10,
        "rs_m": 100,
        "rs_f": 120,
        "ss_m": 200,
        "ss_f": 250
    }
}

pairing1 = [
    10, 20, 30, 40, 50, 60, 70, 80, 90
]

survival_rates1 = {
    'bt': {
        'rr': 1,
        'rs': .5,
        'ss': 0
    },
    'ref': {
        'rr': 0,
        'rs': .5,
        'ss': 1
    }
}

survival_rates2 = {
    'bt': {
        'rr': .25,
        'rs': .025,
        'ss': .05
    },
    'ref': {
        'rr': 0,
        'rs': .5,
        'ss': .2
    }
}
