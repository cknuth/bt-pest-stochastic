from __future__ import division
import unittest
from assets.utils import *
import copy
import sample_data


class TestUtils(unittest.TestCase):

    def test_totals(self):
        f = sample_data.bt_field_instance2
        self.assertEqual(60, rr(f))
        self.assertEqual(1970, rs(f))
        self.assertEqual(7870, ss(f))
        self.assertEqual(9900, total(f))

    def test_calc_freq1(self):
        fields = sample_data.two_fields1
        self.assertEqual(
            (2 * (40 + 30) + 980 + 980 + 99 + 98) / (2 * 11000),
            calc_freq(fields)
        )

    def test_calc_freq2(self):
        fields = sample_data.two_fields2
        self.assertEqual(
            (1020 + 1010 + 99 + 98) / (2 * 11000),
            calc_freq(fields)
        )

    def test_fields_equals(self):
        fields = copy.deepcopy(sample_data.two_fields1)
        self.assertTrue(equals_fields(sample_data.two_fields1, fields))
        self.assertFalse(equals_fields(sample_data.two_fields1, sample_data.two_fields2))

    def test_equals_subpop(self):
        subpop = copy.deepcopy(sample_data.subpop1)
        self.assertTrue(equals_subpop(subpop, sample_data.subpop1))

    def test_grand_total(self):
        self.assertEqual(11000, grand_total(sample_data.two_fields1))

    def test_split(self):
        one, two = split(0)
        self.assertTrue(one == 0 and two == 0)
        one, two = split(100)
        self.assertTrue(one == 50 and two == 50)
        one, two = split(51)
        self.assertTrue(one + two == 51)

    def test_map_by_field(self):
        fields = map_by_field(lambda field: 1, sample_data.two_fields1)
        self.assertEqual(fields[0][0], 1)
        self.assertEqual(fields[0][1], 1)


if __name__ == '__main__':
    unittest.main()