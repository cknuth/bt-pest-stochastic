import copy
import unittest

import sample_data
from assets.model.migration.migrator_two_field import MigratorTwoField
from assets.utils import *


class TestMigrator(unittest.TestCase):

    def test_migrate_rand_no_movement(self):
        fields = copy.deepcopy(sample_data.two_fields1)
        MigratorTwoField(0, 0, True, True).migrate(fields)
        self.assertTrue(equals_fields(fields, sample_data.two_fields1))

    def test_migrate_rand_all_move(self):
        fields = copy.deepcopy(sample_data.two_fields1)
        MigratorTwoField(1, 1, True, True).migrate(fields)
        self.assertTrue(equals_subpop(fields[0][1]['subpop'], sample_data.two_fields1[0][0]['subpop']))
        self.assertTrue(equals_subpop(fields[0][0]['subpop'], sample_data.two_fields1[0][1]['subpop']))

    def test_migrate_rand_some_move(self):
        fields = copy.deepcopy(sample_data.two_fields1)
        MigratorTwoField(.5, .5, True, True).migrate(fields)
        # may fail, but very unlikely
        self.assertFalse(equals_subpop(fields[0][0]['subpop'], sample_data.two_fields1[0][0]['subpop']))
        self.assertFalse(equals_subpop(fields[0][1]['subpop'], sample_data.two_fields1[0][1]['subpop']))
        self.assertEqual(grand_total(fields), grand_total(sample_data.two_fields1))

    # TODO: test deterministic case

if __name__ == '__main__':
    unittest.main()
