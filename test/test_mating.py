from __future__ import division

import unittest

import sample_data
from assets.model.mating import Matchmaker
from assets.utils import *


class TestMating(unittest.TestCase):

    def test_pvals_for_three_type(self):
        pvals = Matchmaker(48, False, True, False, True).__find_pvals_3_type__(sample_data.field_instance1)
        self.assertAlmostEqual(pvals[0], (30 ** 2) / (700 ** 2))
        self.assertAlmostEqual(pvals[1], (30 * 220) / (700 ** 2))
        self.assertAlmostEqual(pvals[2], (30 * 450) / (700 ** 2))
        self.assertAlmostEqual(pvals[3], (220 * 30) / (700 ** 2))
        self.assertAlmostEqual(pvals[4], (220 ** 2) / (700 ** 2))
        self.assertAlmostEqual(pvals[5], (220 * 450) / (700 ** 2))
        self.assertAlmostEqual(pvals[6], (450 * 30) / (700 ** 2))
        self.assertAlmostEqual(pvals[7], (450 * 220) / (700 ** 2))
        self.assertAlmostEqual(pvals[8], (450 ** 2) / (700 ** 2))
        self.assertAlmostEqual(sum(pvals), 1)

    def test_offspring_deterministic_for_three_type(self):
        offspring = Matchmaker(48, False, True, False, True).__get_offspring_deterministic_3_type__(sample_data.pairing1)
        self.assertEqual(offspring['rr_m'] + offspring['rr_f'], 48 * (10 + .5 * 20 + .5 * 40 + .25 * 50))
        self.assertEqual(offspring['rs_m'] + offspring['rs_f'], 48 * (.5 * 20 + 30 + .5 * 40 + .5 * 50 + .5 * 60 + 70 + .5 * 80))
        self.assertEqual(offspring['ss_m'] + offspring['ss_f'], 48 * (.25 * 50 + .5 * 60 + .5 * 80 + 90))

    def test_mate_within_field_rand_pairing_deter_repro_3_type(self):
        field = Matchmaker(48, False, True, False, True).__mate_within_field__(sample_data.field_instance1)
        self.assertEqual(total(field), 48 / 2 * total(sample_data.field_instance1))

    def test_mate_rand_pairing_deter_repro_3_type(self):
        fields = Matchmaker(48, False, True, False, True).mate(sample_data.two_fields1)
        self.assertEqual(total(fields[0][0]), 48 / 2 * total(sample_data.two_fields1[0][0]))
        self.assertEqual(total(fields[0][1]), 48 / 2 * total(sample_data.two_fields1[0][1]))
        # possibly can fail, but extremely unlikely
        self.assertTrue(fields[0][1]['subpop']['rr_m'] > 0)
        self.assertTrue(fields[0][1]['subpop']['rr_f'] > 0)

if __name__ == '__main__':
    unittest.main()
