from __future__ import division

import unittest

import sample_data
from assets.model.pesticide import Pesticide


class TestPesticide(unittest.TestCase):

    def test_pesticide_rand(self):
        fields = Pesticide(sample_data.survival_rates1, True, True).kill(sample_data.two_fields1)
        # possible that a couple of these tests will fail, but extremely unlikely
        self.assertEqual(fields[0][0]['subpop']['rr_m'], 40)
        self.assertEqual(fields[0][0]['subpop']['rr_f'], 30)
        self.assertTrue(300 < fields[0][0]['subpop']['rs_m'] < 700)
        self.assertTrue(300 < fields[0][0]['subpop']['rs_f'] < 700)
        self.assertEqual(fields[0][0]['subpop']['ss_m'], 0)
        self.assertEqual(fields[0][0]['subpop']['ss_f'], 0)
        self.assertEqual(fields[0][1]['subpop']['rr_m'], 0)
        self.assertEqual(fields[0][1]['subpop']['rr_f'], 0)
        self.assertTrue(25 < fields[0][1]['subpop']['rs_m'] < 75)
        self.assertTrue(25 < fields[0][1]['subpop']['rs_f'] < 75)
        self.assertEqual(fields[0][1]['subpop']['ss_m'], 402)
        self.assertEqual(fields[0][1]['subpop']['ss_f'], 401)

    def test_pesticide_not_rand_stochastic(self):
        fields = Pesticide(sample_data.survival_rates2, False, True).kill(sample_data.two_fields1)
        # possible that a couple of these tests will fail, but extremely unlikely
        self.assertEqual(fields[0][0]['subpop']['rr_m'], 40 // 4)
        self.assertEqual(fields[0][0]['subpop']['rr_f'], 30 // 4)
        self.assertEqual(fields[0][0]['subpop']['rs_m'], 980 // 40)
        self.assertEqual(fields[0][0]['subpop']['rs_f'], 980 // 40)
        self.assertEqual(fields[0][0]['subpop']['ss_m'], 3980 // 20)
        self.assertEqual(fields[0][0]['subpop']['ss_f'], 3990 // 20)
        self.assertEqual(fields[0][1]['subpop']['rr_m'], 0)
        self.assertEqual(fields[0][1]['subpop']['rr_f'], 0)
        self.assertEqual(fields[0][1]['subpop']['rs_m'], 99 // 2)
        self.assertEqual(fields[0][1]['subpop']['rs_f'], 98 // 2)
        self.assertEqual(fields[0][1]['subpop']['ss_m'], 402 // 5)
        self.assertEqual(fields[0][1]['subpop']['ss_f'], 401 // 5)

    def test_pesticide_not_rand_deterministic(self):
        fields = Pesticide(sample_data.survival_rates2, False, False).kill(sample_data.two_fields1)
        # possible that a couple of these tests will fail, but extremely unlikely
        self.assertEqual(fields[0][0]['subpop']['rr_m'], 40 / 4)
        self.assertEqual(fields[0][0]['subpop']['rr_f'], 30 / 4)
        self.assertEqual(fields[0][0]['subpop']['rs_m'], 980 / 40)
        self.assertEqual(fields[0][0]['subpop']['rs_f'], 980 / 40)
        self.assertEqual(fields[0][0]['subpop']['ss_m'], 3980 / 20)
        self.assertEqual(fields[0][0]['subpop']['ss_f'], 3990 / 20)
        self.assertEqual(fields[0][1]['subpop']['rr_m'], 0)
        self.assertEqual(fields[0][1]['subpop']['rr_f'], 0)
        self.assertEqual(fields[0][1]['subpop']['rs_m'], 99 / 2)
        self.assertEqual(fields[0][1]['subpop']['rs_f'], 98 / 2)
        self.assertEqual(fields[0][1]['subpop']['ss_m'], 402 / 5)
        self.assertEqual(fields[0][1]['subpop']['ss_f'], 401 / 5)


if __name__ == '__main__':
    unittest.main()