import unittest

from assets.model.population_initializer import PopulationInitializer
from assets.utils import *


class TestPopulationInitializer(unittest.TestCase):

    @staticmethod
    def stochastic_two_field():
        return PopulationInitializer(.1, [[10000, 1000]], [[True, False]], True)

    @staticmethod
    def deterministic_two_field():
        return PopulationInitializer(.1, [[10000, 1000]], [[True, False]], False)

    def test_deterministic_two_field(self):
        p = self.deterministic_two_field()
        fields = p.init()
        self.assertEqual(1000, fields[0][0]['subpop']['rs_m'])
        self.assertEqual(1000, fields[0][0]['subpop']['rs_f'])
        self.assertEqual(4000, fields[0][0]['subpop']['ss_m'])
        self.assertEqual(4000, fields[0][0]['subpop']['ss_f'])
        self.assertEqual(100, fields[0][1]['subpop']['rs_m'])
        self.assertEqual(100, fields[0][1]['subpop']['rs_f'])
        self.assertEqual(400, fields[0][1]['subpop']['ss_m'])
        self.assertEqual(400, fields[0][1]['subpop']['ss_f'])
        self.assertEqual(.1, calc_freq(fields))
        self.assertEqual(p.capacity[0][0], total(fields[0][0]))
        self.assertEqual(p.capacity[0][1], total(fields[0][1]))
        self.assertEqual(p.bt_field_layout[0][0], fields[0][0]['bt'])
        self.assertEqual(p.bt_field_layout[0][1], fields[0][1]['bt'])

    def test_stochastic_two_fields(self):
        p = self.stochastic_two_field()
        fields = p.init()
        self.assertEqual(1000, fields[0][0]['subpop']['rs_m'])
        self.assertEqual(1000, fields[0][0]['subpop']['rs_f'])
        self.assertEqual(4000, fields[0][0]['subpop']['ss_m'])
        self.assertEqual(4000, fields[0][0]['subpop']['ss_f'])
        self.assertEqual(100, fields[0][1]['subpop']['rs_m'])
        self.assertEqual(100, fields[0][1]['subpop']['rs_f'])
        self.assertEqual(400, fields[0][1]['subpop']['ss_m'])
        self.assertEqual(400, fields[0][1]['subpop']['ss_f'])
        self.assertEqual(.1, calc_freq(fields))
        self.assertEqual(p.capacity[0][0], total(fields[0][0]))
        self.assertEqual(p.capacity[0][1], total(fields[0][1]))
        self.assertEqual(p.bt_field_layout[0][0], fields[0][0]['bt'])
        self.assertEqual(p.bt_field_layout[0][1], fields[0][1]['bt'])

    def test_stochastic_two_fields_slightly_off(self):
        p = self.stochastic_two_field()
        p.init_freq = .0999
        fields = p.init()
        self.assertTrue(998 <= fields[0][0]['subpop']['rs_m'])
        self.assertTrue(998 <= fields[0][0]['subpop']['rs_f'])
        self.assertTrue(3998 <= fields[0][0]['subpop']['ss_m'])
        self.assertTrue(3998 <= fields[0][0]['subpop']['ss_f'])
        self.assertTrue(98 <= fields[0][1]['subpop']['rs_m'])
        self.assertTrue(98 <= fields[0][1]['subpop']['rs_f'])
        self.assertTrue(398 <= fields[0][1]['subpop']['ss_m'])
        self.assertTrue(398 <= fields[0][1]['subpop']['ss_f'])
        self.assertTrue(.0998 < calc_freq(fields) <= .0999)
        self.assertEqual(p.capacity[0][0], total(fields[0][0]))
        self.assertEqual(p.capacity[0][1], total(fields[0][1]))
        self.assertEqual(p.bt_field_layout[0][0], fields[0][0]['bt'])
        self.assertEqual(p.bt_field_layout[0][1], fields[0][1]['bt'])

if __name__ == '__main__':
    unittest.main()

