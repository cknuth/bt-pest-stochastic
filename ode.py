from __future__ import division
from assets.file_io import *
from analyzer import analyze_data


# this has not been rewritten for mongo db interaction
def main():
    # initialize
    config_module = sys.argv[1]
    config = __import__(sys.argv[1], globals(), locals(), ['init_freqs'])
    init_freqs = [float(freq) for freq in sys.argv[2].split(',')]
    num_sim = int(sys.argv[3])

    for init_freq in init_freqs:
        run_simulations(config, config_module, init_freq, num_sim)


def run_simulations_mongo(db, run_instance_id, model_config, run_config):
    print('running simulation for initial frequency: ' + str(model_config['frequency']))
    runs = []
    det_runs = []

    fecundity = model_config['fecundity']
    N = model_config['capacity'][0][0]
    s1 = model_config['survival_rate']['bt']['ss']
    s2 = model_config['survival_rate']['bt']['rs']
    s3 = model_config['survival_rate']['bt']['rr']

    # stochastic version
    if (model_config['rand']['pesticide'] and not model_config['rand']['mating']) or \
            (model_config['rand']['mating'] and not model_config['rand']['pesticide']):
        for j in range(run_config['number_of_sims']):
            run = [model_config['frequency']]
            # calculate curve
            i = 0
            while (1 / N**4) <= run[i] < run_config['threshold']:
                if model_config['rand']['pesticide'] and not model_config['rand']['mating']:
                    run.append(diff_eq(run[i], s1, s2, s3) + noise_factor(run[i], s2, s3, fecundity, N, model='01') + run[i])
                elif model_config['rand']['mating'] and not model_config['rand']['pesticide']:
                    run.append(diff_eq(run[i], s1, s2, s3) + noise_factor(run[i], s2, s3, fecundity, N, model='10') + run[i])
                else:
                    raise Exception('unexpected model')
                i += 1
            runs.append(run)
            if j % 100 == 0:
                sys.stdout.write('.')
                sys.stdout.flush()

    # deterministic version
    run = [model_config['frequency']]
    i = 0
    while (1 / N**4) <= run[i] < run_config['threshold']:
        run.append(diff_eq(run[i], s1, s2, s3) + run[i])
        i += 1
    det_runs.append(run)

    ode_analysis = analyze_data(det_runs)
    run_instance = db.run_instance.find_one({'_id': run_instance_id})
    db.run_instance.update_one(
        {'_id': run_instance['_id']},
        {'$set': {'ode_analysis': ode_analysis}}
    )
    if (model_config['rand']['pesticide'] and not model_config['rand']['mating']) or \
            (model_config['rand']['mating'] and not model_config['rand']['pesticide']):
        sde_analysis = analyze_data(runs)
        db.run_instance.update_one(
            {'_id': run_instance['_id']},
            {'$set': {'sde_analysis': sde_analysis}}
        )


def run_simulations(config, config_module, init_freq, num_sim):
    print('running simulation for initial frequency: ' + str(init_freq))
    runs = []

    fecundity = config.fecundity
    N = config.capacity[0][0]
    s1 = config.survival_rate['bt']['ss']
    s2 = config.survival_rate['bt']['rs']
    s3 = config.survival_rate['bt']['rr']
    stochastic = config.stochastic

    for j in range(num_sim):
        run = [init_freq]
        # calculate curve
        i = 0
        while (1 / N) <= run[i] < 0.95:
            if stochastic:
                run.append(diff_eq(run[i], s1, s2, s3) + noise_factor(run[i], s2, s3, fecundity, N) + run[i])
            else:
                run.append(diff_eq(run[i], s1, s2, s3) + run[i])
            i += 1
        runs.append(run)
        if j % 100 == 0:
            sys.stdout.write('.')
            sys.stdout.flush()
    print('')

    print('writing results...')
    # plot
    write_csv(runs, init_freq, config_module, num_sim)
    figure(1)
    xlabel('generation')
    ylabel('frequency of resistant gene')
    for i in range(num_sim if num_sim < 20 else 20):
        plot(runs[i], 'r')
    axis([0, max(map(lambda run: len(run), runs[0:num_sim if num_sim < 20 else 20])), 0, 1])
    savefig('ode/result.png')
    close()
    figure(1)
    xlabel('generation')
    ylabel('frequency of resistant gene')
    for i in range(num_sim if num_sim < 20 else 20):
        plot(runs[i], 'r')
    axis([0, max(map(lambda run: len(run), runs[0:num_sim if num_sim < 20 else 20])), 0, .05])
    savefig('ode/result_small_scale.png')
    close()


def diff_eq(freq, s1, s2, s3):
    return ((s2 - s1) * freq + (s3 - 3*s2 + 2*s1) * freq**2 - (s3 - 2*s2 + s1) * freq**3) \
           / (s1 + 2*(s2 - s1)*freq + (s3 - 2*s2 + s1)*freq**2)


def noise_factor(freq, s2, s3, fecundity, N, model='01'):
    if model == '01':
        stdev = (fecundity / (4 * N) * (s2 * (1 - s2) * freq * (1 - freq) + 2 * s3 * (1 - s3) * freq ** 2)) ** (1 / 2)
        return np.random.normal(0, stdev, 1)[0]
    elif model == '10':
        stdev = (fecundity / 4) * freq * (1 - freq)
        return np.random.normal(0, stdev, 1)[0]
    else:
        raise Exception('unknown model')


if __name__ == '__main__':
    main()
