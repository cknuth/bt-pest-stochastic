from __future__ import division
from pymongo import MongoClient
import numpy as np
import matplotlib.pyplot as plt
from bson.objectid import ObjectId
from bt_pest_model import run_test
from copy import deepcopy


# vary over N for three different survival rates with fixed initial frequency
def run_simulations():
    client = MongoClient()
    db = client['bt-pest-model']
    confs = db.configurations.find({'collection': 'N_RANGE_TEST_5'}, no_cursor_timeout=True)
    cc = db.conf_collection.find_one({'collection_name': 'N_RANGE_TEST_5'})
    c_run = cc['collection_runs'] + 1
    params = db.run_params.find_one({'_id': ObjectId('5d8f7a94c77a114204097dbc')})

    for conf in confs:
        run_test(db, conf, params, c_run)

    db.conf_collection.update_one(
        {'_id': cc['_id']},
        {'$set': {'collection_runs': c_run}}
    )
    confs.close()


# for N_RANGE_TEST_5
def graph_data_N_RANGE_TEST_5(collection_run):
    client = MongoClient()
    db = client['bt-pest-model']
    confs = db.run_instance.find({'model_config.collection': 'N_RANGE_TEST_5', 'collection_run': collection_run})
    t_sde_1 = []
    t_sde_2 = []
    t_sde_3 = []
    died = [0, 0, 0]
    for c in confs:
        if abs(c['model_config']['survival_rate']['bt']['ss'] - 1) < 0.05 and \
                abs(c['model_config']['survival_rate']['bt']['rs'] - 0.2) < 0.05:
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde_1.append([c['model_config']['capacity'][0][0], c['sde_analysis']['saturation_exp']])
            else:
                died[0] +=1
        elif abs(c['model_config']['survival_rate']['bt']['ss'] - 0.2) < 0.05 and \
                abs(c['model_config']['survival_rate']['bt']['rs'] - 0.1) < 0.05:
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde_2.append([c['model_config']['capacity'][0][0], c['sde_analysis']['saturation_exp']])
            else:
                died[1] +=1
        elif abs(c['model_config']['survival_rate']['bt']['ss'] - 1) < 0.05 and \
                abs(c['model_config']['survival_rate']['bt']['rs'] - 0.1) < 0.05:
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde_3.append([c['model_config']['capacity'][0][0], c['sde_analysis']['saturation_exp']])
            else:
                died[2] +=1
        else:
            print('unexpected value')
    t_sde_1 = np.array(t_sde_1)
    t_sde_2 = np.array(t_sde_2)
    t_sde_3 = np.array(t_sde_3)
    print(died)
    # plt.figure(1)
    # plt.plot(t_sde_1[:,0], t_sde_1[:,1])
    # plt.title('(w_ss, w_rs, w_rr) = (1, 0.2, 0.1), (01) model')
    # plt.xlabel('N')
    # plt.ylabel('t_sde')
    # plt.show()
    #
    # plt.figure(2)
    # plt.plot(t_sde_2[:,0], t_sde_2[:,1])
    # plt.title('(w_ss, w_rs, w_rr) = (0.2, 0.1, 1), (01) model')
    # plt.xlabel('N')
    # plt.ylabel('t_sde')
    # plt.show()
    #
    # plt.figure(3)
    # plt.plot(t_sde_3[:,0], t_sde_3[:,1])
    # plt.title('(w_ss, w_rs, w_rr) = (1, 0.1, 0.2), (01) model')
    # plt.xlabel('N')
    # plt.ylabel('t_sde')
    # plt.show()


# for N_RANGE_TEST_4
def graph_data(collection_run):
    client = MongoClient()
    db = client['bt-pest-model']
    confs = db.run_instance.find({'model_config.collection': 'N_RANGE_TEST_4', 'collection_run': collection_run})
    t_ode_1 = []
    t_sde_1 = []
    t_ode_2 = []
    t_sde_2 = []
    for c in confs:
        if abs(c['model_config']['survival_rate']['bt']['rs'] - 0.1) < 0.05:
            t_ode_1.append([c['model_config']['capacity'][0][0], c['ode_analysis']['saturation_exp']])
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde_1.append([c['model_config']['capacity'][0][0], c['sde_analysis']['saturation_exp']])
        elif abs(c['model_config']['survival_rate']['bt']['rs'] - 0.2) < 0.05:
            t_ode_2.append([c['model_config']['capacity'][0][0], c['ode_analysis']['saturation_exp']])
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde_2.append([c['model_config']['capacity'][0][0], c['sde_analysis']['saturation_exp']])
        else:
            print('unexpected value')
    t_ode_1 = np.array(t_ode_1)
    t_sde_1 = np.array(t_sde_1)
    t_ode_2 = np.array(t_ode_2)
    t_sde_2 = np.array(t_sde_2)

    plt.figure(1)
    plt.plot(np.sqrt(t_sde_1[:,0]), t_sde_1[:,1])
    plt.title('w_rs = 0.1, t_sde, (01) model')
    plt.xlabel('sqrt(N)')
    plt.ylabel('t_sde')
    plt.show()

    plt.figure(2)
    plt.plot(t_ode_1[:,0], t_ode_1[:,1])
    plt.title('t_ode, w_rs = 0.1, (01) model')
    plt.xlabel('N')
    plt.ylabel('t_ode')
    plt.show()

    plt.figure(3)
    plt.plot(np.log10(t_ode_2[:, 0]), t_ode_2[:, 1])
    plt.plot(np.log10(t_sde_2[:,0]), t_sde_2[:,1])
    plt.legend(('t_ode', 't_sde'), loc='lower right')
    plt.xlabel('log10(N)')
    plt.ylabel('t')
    plt.title('w_rs = 0.2, (01) model')
    plt.show()


def setup_data():
    #print('data already set up')
    #return

    client = MongoClient()
    db = client['bt-pest-model']
    mc = db.configurations.find_one({'_id': ObjectId('5aef2699ec925e0037b3de24')})

    mc.pop('_id', None)
    mc['collection'] = 'N_RANGE_TEST_5'
    mc.pop('notes', None)

    c_ids = []

    max_N = 1e7
    confs = []
    for w in [[1, 0.2, 0.1], [0.2, 0.1, 1], [1, 0.1, 0.2]]:
        for cap in np.square(np.linspace(0, np.sqrt(max_N), 100)):
            if cap < 30:
                # skip the zero sized field
                pass
            else:
                c = deepcopy(mc)
                c['survival_rate']['bt']['ss'] = w[0]
                c['survival_rate']['ref']['ss'] = w[0]
                c['survival_rate']['bt']['rs'] = w[1]
                c['survival_rate']['ref']['rs'] = w[1]
                c['survival_rate']['bt']['rr'] = w[2]
                c['survival_rate']['ref']['rr'] = w[2]
                c['capacity'][0][0] = cap
                c['frequency'] = 1 / cap
                c['rand']['pesticide'] = True
                c['rand']['mating'] = False
                c_ids.append(db.configurations.insert_one(c).inserted_id)

    db.conf_collection.insert_one(
        {
            'collection_name': 'N_RANGE_TEST_5',
            'conf_ids': c_ids,
            'collection_runs': 0
        }
    )
