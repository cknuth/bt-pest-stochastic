from __future__ import division
from pymongo import MongoClient
from bson.objectid import ObjectId
import numpy as np
import copy
from bt_pest_model import run_test


COLLECTION_NAME = 'SPECTRUM_SURVIVAL_9'


# varying over survival rates with fixed N and initial frequency
def run_simulations():
    client = MongoClient()
    db = client['bt-pest-model']
    confs = db.configurations.find({'collection': COLLECTION_NAME}, no_cursor_timeout=True)
    cc = db.conf_collection.find_one({'collection_name': COLLECTION_NAME}, no_cursor_timeout=True)
    run_instances = db.run_instance.find({})
    already_ran_configs = [inst['model_config']['_id'] if 'model_config' in inst.keys() else 0 for inst in run_instances]
    #c_run = cc['collection_runs'] + 1
    c_run = cc['collection_runs']
    #params = db.run_params.find_one({'_id': ObjectId('5d8f7a94c77a114204097dbc')})
    #params = db.run_params.find_one()

    params = {
        "threshold": 0.5,
        "frequencies": [0.0001],
        "number_of_sims": 10000
    }

    for conf in confs:
        if conf['_id'] in already_ran_configs:
            print('run already exists for config: ' + str(conf['_id']))
        else:
            print('running config: ' + str(conf['_id']))
            run_test(db, conf, params, c_run)

    #db.conf_collection.update_one(
    #    {'_id': cc['_id']},
    #    {'$set': {'collection_runs': c_run}}
    #)
    confs.close()


def synthesize_data():
    client = MongoClient()
    db = client['bt-pest-model']
    run_instances = db.run_instance.find({'model_config.collection': COLLECTION_NAME})
    rand_pesticide = []
    rand_mating = []
    rand_mating_pesticide = []

    header_written = False
    header = ['s', 'h']

    for ri in run_instances:
        s = 1 - ri['model_config']['survival_rate']['bt']['ss']
        h = (ri['model_config']['survival_rate']['bt']['rs'] - 1 + s) / s
        s = np.around(s, decimals=1)
        h = np.around(h, decimals=1)
        if 'analysis_by_frequency' in ri and len(ri['analysis_by_frequency']) > 0:
            result = np.zeros(2+len(ri['analysis_by_frequency'][0]['analysis'].keys()))
            result[0] = s
            result[1] = h
            for i, key in enumerate(ri['analysis_by_frequency'][0]['analysis'].keys()):
                if not header_written:
                    header.append(str(key))
                result[2+i] = ri['analysis_by_frequency'][0]['analysis'][key]
            if ri['model_config']['rand']['pesticide'] and ri['model_config']['rand']['mating']:
                rand_mating_pesticide.append(result)
            elif ri['model_config']['rand']['pesticide']:
                rand_pesticide.append(result)
            else:
                rand_mating.append(result)
            header_written = True

    header_str = ','.join(header)
    np.savetxt('out/rand_mating_pesticide.csv', np.array(rand_mating_pesticide), fmt='%10.5f', delimiter=',', header=header_str)
    np.savetxt('out/rand_mating.csv', np.array(rand_mating), fmt='%10.5f', delimiter=',', header=header_str)
    np.savetxt('out/rand_pesticide.csv', np.array(rand_pesticide), fmt='%10.5f', delimiter=',', header=header_str)


def setup_data():
    default_conf = {
        "rand": {
            "pesticide":True,
            "initialization":False,
            "competition":False,
            "migration":False,
            "reproduction":False,
            "mating":False
        },
        "fecundity":24,
        "capacity":[[10000]],
        "survival_rate":{
            "bt":{
                "ss":0.1,
                "rr":1,
                "rs":0.1
            },
            "ref":{
                "ss":0.1,
                "rr":1,
                "rs":0.1
            }
        },
        "bt_field_layout":[[True]],
        "gendered":False,
        "migration_scheme":"ONE_FIELD",
        "allows_continuous":False,
        "collection": COLLECTION_NAME
    }

    client = MongoClient()
    db = client['bt-pest-model']

    c_ids = []
    N = 10000
    for s in np.multiply(range(1, 11), 0.1):
        for h in np.multiply(range(0, 11), 0.1):
            for sim_type in ['rand_pest', 'rand_mat', 'rand_pest_mat']:
                new_conf = copy.deepcopy(default_conf)
                if sim_type == 'rand_pest':
                    new_conf['rand']['pesticide'] = True
                    new_conf['rand']['mating'] = False
                elif sim_type == 'rand_mat':
                    new_conf['rand']['pesticide'] = False
                    new_conf['rand']['mating'] = True
                elif sim_type == 'rand_pest_mat':
                    new_conf['rand']['mating'] = True
                    new_conf['rand']['pesticide'] = True
                new_conf['survival_rate']['bt']['rs'] = h + (1-h)*(1-s)
                new_conf['survival_rate']['ref']['rs'] = h + (1-h)*(1-s)
                new_conf['survival_rate']['bt']['ss'] = 1 - s
                new_conf['survival_rate']['ref']['ss'] = 1 - s
                new_conf['note'] = 's: ' + str(s) + ' h: ' + str(h)
                new_conf['capacity'][0][0] = N
                new_conf['frequency'] = 1 / N
                c_ids.append(db.configurations.insert_one(new_conf).inserted_id)

    db.conf_collection.insert_one(
        {
            'collection_name': COLLECTION_NAME,
            'conf_ids': c_ids,
            'collection_runs': 0
        }
    )

    #db.configurations.insert_many(confs)


if __name__ == '__main__':
    setup_data()
    #run_simulations()
    #synthesize_data()
