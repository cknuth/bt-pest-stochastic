# DO NOT MODIFY THIS FILE

import assets.model.migration.migration_scheme

# a list of initial frequency values. Simulations will be run for each of the initial frequencies
init_freqs = [0.0001, 0.0005, 0.001, 0.002, 0.003, 0.004, 0.005, 0.01, 0.05, 0.1]
# the number of offspring produced by each pair
fecundity = 48
# 2-d array listing the capacity of each field. The layout of the fields follows the 2-d structure
capacity = [[10000, 1000]]
# 2-d array indicating whether or not a particular field has pesticide or is a refuge field
bt_field_layout = [[True, False]]
# the name of the migration scheme used
migration_scheme = assets.model.migration.migration_scheme.TWO_FIELD
# location for extra args that the migration scheme may need
migration_args = {
    'bt_emigration_rate': .01,
    'ref_emigration_rate': .1
}
# survival rates in the bt and refuge fields in the face of pesticide
survival_rate = {
    'bt': {
        'rr': 1,
        'rs': .1,
        'ss': .1
    },
    'ref': {
        'rr': 1,
        'rs': .1,
        'ss': .1
    }
}
# whether or not the model should be run with gender
gendered = False
# flag for each step if it should be run with randomness
rand = {
    'initialization': False,  # not used at the moment
    'migration': True,
    'mating': True,
    'reproduction': False,
    'pesticide': True,
    'competition': False
}
# flag for if the model is deterministic. If True, the above fields MUST all be false.
# Otherwise the behavior is uncertain.
# This is separated out into its own flag to indicate that the model accepts continuous values for pests
stochastic = True
