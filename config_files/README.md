The config files in this directory are referenced by the analyzer to reduce duplication
in each output file. As such, the config files MUST NOT be changed because that would
lead to ambiguity.

If you want to make a new configuration, make a new file and reference that.