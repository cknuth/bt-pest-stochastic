from config_files.model_types import *

version = 5

# initial frequency should be provided on the command line
# the number of offspring produced by each pair
fecundity = 48
# 2-d array listing the capacity of each field. The layout of the fields follows the 2-d structure
capacity = [[10000]]
# 2-d array indicating whether or not a particular field has pesticide or is a refuge field
bt_field_layout = [[True]]
# the name of the migration scheme used
migration_scheme = ONE_FIELD_MIGRATION
# location for extra args that the migration scheme may need
migration_args = {}
# survival rates in the bt and refuge fields in the face of pesticide
survival_rate = {
    'bt': {
        'rr': 1,
        'rs': .1,
        'ss': .1
    },
    'ref': {
        'rr': 1,
        'rs': .1,
        'ss': .1
    }
}
# whether or not the model should be run with gender
gendered = False
# flag for each step if it should be run with randomness
rand = {
    'initialization': DETERMINISTIC,  # not used at the moment
    'migration': DETERMINISTIC,
    'mating': DETERMINISTIC,
    'reproduction': DETERMINISTIC,
    'pesticide': STOCHASTIC,
    'competition': DENSITY_DEPENDENT_DETERMINISTIC_WITH_ROUND
}
# flag for if the model is deterministic. If True, the above fields MUST all be false.
# Otherwise the behavior is uncertain.
# This is separated out into its own flag to indicate that the model accepts continuous values for pests
stochastic = True
