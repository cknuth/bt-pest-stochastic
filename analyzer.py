from assets.file_io import *
from pymongo import MongoClient
from bson.objectid import ObjectId
import sys


def main():
    client = MongoClient()
    db = client['bt-pest-model']

    analyze_and_write_all_runs(db, ObjectId(sys.argv[1]))


def analyze_all_runs(frequencies_list, initial_frequency_list, det_run_time=-1):
    analysis_by_frequency = [
        {
            'initial_frequency': initial_frequency_list[i],
            'analysis': analyze_data(frequencies_list[i], det_run_time)
        } for i in range(len(frequencies_list))
    ]
    return analysis_by_frequency


def analyze_and_write_all_runs(db, run_instance_id, frequencies_list, initial_frequency_list, det_run_time=-1, det_saturated=False):
    run_instance = db.run_instance.find_one({'_id': run_instance_id}, no_cursor_timeout=True)
    analysis_by_frequency = [
        {
            'initial_frequency': initial_frequency_list[i],
            'analysis': analyze_data(frequencies_list[i], det_run_time, det_saturated)
        } for i in range(len(frequencies_list))
    ]
    db.run_instance.update_one(
        {'_id': run_instance['_id']},
        {'$set': {'analysis_by_frequency': analysis_by_frequency}}
    )


def analyze_data(runs, det_run_time=-1, det_saturated=False):
    lowers = []
    results = {}
    results['num_runs'] = len(runs)
    results['simple_threshold_exp'] = 0
    results['saturation_exp'] = 0
    results['extinction_exp'] = 0
    results['fixation_exp'] = 0
    results['num_saturated'] = 0
    results['num_extinct'] = 0
    results['saturation_max'] = -1
    results['saturation_min'] = sys.maxint
    results['extinction_max'] = -1
    results['extinction_min'] = sys.maxint
    if det_saturated:
        results['num_faster_than_det'] = 0
    else:
        results['num_faster_than_det'] = -1  # -1 indicates the deterministic run did not saturate
    for run in runs:
        lower = run[0]
        tau = 0
        length = run.__len__() - 1
        results['fixation_exp'] += length
        if run[length] > .5:
            results['num_saturated'] += 1
            for i in range(run.__len__()):
                if i > 0 and run[i] < run[i - 1]:
                    lower = run[i]
                    tau = i
            lowers.append(lower)
            results['simple_threshold_exp'] += tau
            results['saturation_exp'] += length
            if length > results['saturation_max']:
                results['saturation_max'] = length
            if length < results['saturation_min']:
                results['saturation_min'] = length
            if det_saturated and length < det_run_time:
                results['num_faster_than_det'] += 1
        else:
            results['num_extinct'] += 1
            results['extinction_exp'] += length
            if length > results['extinction_max']:
                results['extinction_max'] = length
            if length < results['extinction_min']:
                results['extinction_min'] = length
    if results['num_saturated'] > 0:
        results['simple_threshold_exp'] = float(results['simple_threshold_exp']) / results['num_saturated']
        results['saturation_exp'] = float(results['saturation_exp']) / results['num_saturated']
    else:
        results['simple_threshold_exp'] = -1
        results['saturation_exp'] = -1
    if results['num_extinct'] > 0:
        results['extinction_exp'] = float(results['extinction_exp']) / results['num_extinct']
    else:
        results['extinction_exp'] = -1
    if len(runs) > 0:
        results['prob_of_saturation'] = float(results['num_saturated']) / (results['num_saturated'] + results['num_extinct'])
        results['fixation_exp'] = float(results['fixation_exp']) / len(runs)
    else:
        results['prob_of_saturation'] = None
        results['fixation_exp'] = None

    return results


if __name__ == '__main__':
    main()
