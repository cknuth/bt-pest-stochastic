import os
import csv
from pylab import *


# first version
# then initial frequency
# then config file
# then frequency of each run
def write_csv(runs, init_freq, config_module, num_runs):
    if not os.path.exists('staging/batch_run/' + str(init_freq)):
        os.makedirs('staging/batch_run/' + str(init_freq))
    filename = 'staging/batch_run/' + str(init_freq) + '/frequency.csv'
    with open(filename, 'wb') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(['version 4'])
        writer.writerow([init_freq])
        writer.writerow([config_module])
        for i in range(num_runs):
            writer.writerow(runs[i])


def graph_last_run(population, freq, init_freq, config_module):
    # then graph
    figure(1, (14, 8))
    subplot(221)
    text(.1, .2,
         'initial freq of resistance = ' + str(init_freq)
         + '\nconfiguration = ' + config_module)
    subplot(222)
    xlabel('generation')
    ylabel('frequency of resistant gene')
    plot(freq, 'r')
    axis([0, population.shape[0], 0, 1])
    subplot(223)
    xlabel('generation')
    ylabel('log of population size')
    plot(np.log(population[:, 0]), 'b', label='RR')
    plot(np.log(population[:, 1]), 'g', label='RS')
    plot(np.log(population[:, 2]), 'r', label='SS')
    legend(loc='upper left')
    subplot(224)
    xlabel('generation')
    ylabel('frequency of resistant gene')
    axis([0, population.shape[0], 0, 0.05])
    plot(freq, 'r')

    filename = determine_filename(init_freq, 1)
    savefig('staging/single_run/' + filename)
    close()


def determine_filename(init_freq, i):
    if os.path.exists('/single_run/' + str(init_freq) + '_' + str(i) + '.png') or \
            os.path.exists('staging/single_run/' + str(init_freq) + '_' + str(i) + '.png'):
        return determine_filename(init_freq, i+1)
    else:
        return str(init_freq) + '_' + str(i) + '.png'


def graph_runs(runs, init_freq, config_module, num_runs):
    max_generations = max([sublist[0].shape[0] for sublist in runs])
    # then graph
    figure(1, (14, 8))
    subplot(221)
    text(.1, .2,
         'initial freq of resistance = ' + str(init_freq)
         + '\nconfiguration = ' + config_module)
    subplot(222)
    xlabel('generation')
    ylabel('frequency of resistant gene')
    axis([0, max_generations, 0, 1])
    for i in range(num_runs):
        plot(runs[i][1], 'r')
    subplot(223)
    xlabel('generation')
    ylabel('log of population size')
    for i in range(num_runs):
        if i == 0:
            plot(np.log(runs[i][0][:, 0]), 'b', label='RR')
            plot(np.log(runs[i][0][:, 1]), 'g', label='RS')
            plot(np.log(runs[i][0][:, 2]), 'r', label='SS')
        else:
            plot(np.log(runs[i][0][:, 0]), 'b')
            plot(np.log(runs[i][0][:, 1]), 'g')
            plot(np.log(runs[i][0][:, 2]), 'r')
    legend(loc='upper left')
    subplot(224)
    xlabel('generation')
    ylabel('frequency of resistant gene')
    axis([0, max_generations, 0, 0.05])
    for i in range(num_runs):
        plot(runs[i][1], 'r')

    if not os.path.isdir('staging/batch_run/' + str(init_freq)):
        os.makedirs('staging/batch_run/' + str(init_freq))
    savefig('staging/batch_run/' + str(init_freq) + '/' + str(num_runs) + '_runs.png')
    close()


def read_frequency_file(filename):
    runs = []
    params = {}
    with open(filename, 'rb') as csv_file:
        reader = csv.reader(csv_file)
        version = reader.next()
        if version[0] == 'version 1':
            print 'version 1 data files are not supported'
            return
        elif version[0] == 'version 2':
            params['version'] = 2
            params['type'] = '12 type'  # all version 2 csv's were 12 type
            params['init_freq'], params['bt_capacity'], params['ref_capacity'], params['init_size'], \
            params['ref_migration'], params['bt_migration'], params['fecundity'], params['ss_survival_rate'], \
            params['rs_survival_rate'], params['rr_survival_rate'] = [float(val) for val in reader.next()]
            for row in reader:
                runs.append([float(val) for val in row])
            return runs, params
        elif version[0] == 'version 3':
            params['version'] = 3
            params['type'] = reader.next()[0]
            params['init_freq'], params['bt_capacity'], params['ref_capacity'], params['init_size'], \
            params['ref_migration'], params['bt_migration'], params['fecundity'], params['ss_survival_rate'], \
            params['rs_survival_rate'], params['rr_survival_rate'] = [float(val) for val in reader.next()]
            for row in reader:
                runs.append([float(val) for val in row])
            return runs, params
        elif version[0] == 'version 4':
            params['version'] = 4
            params['init_freq'] = reader.next()[0]
            params['config_module'] = reader.next()[0]
            for row in reader:
                runs.append([float(val) for val in row])
            return runs, params
        else:
            print 'unrecognized data version: ' + version[0]
            return


def determine_output_path(init_freq, filename, ext, i):
    if not os.path.exists('/batch_run/' + str(init_freq) + '/' + filename + '.' + ext) and \
            not os.path.exists('staging/batch_run/' + str(init_freq) + '/' + filename + '.' + ext):
        return 'staging/batch_run/' + str(init_freq) + '/' + filename + '.' + ext
    elif not os.path.exists('/batch_run/' + str(init_freq) + '/' + filename + '_' + str(i) + '.' + ext) and \
            not os.path.exists('staging/batch_run/' + str(init_freq) + '/' + filename + '_' + str(i) + '.' + ext):
        return 'staging/batch_run/' + str(init_freq) + '/' + filename + '_' + str(i) + '.' + ext
    else:
        return determine_output_path(init_freq, filename, ext, i + 1)


def ensure_path_exists(path):
    if not os.path.isdir(path):
        os.makedirs(path)

