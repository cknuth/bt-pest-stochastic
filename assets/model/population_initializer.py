from __future__ import division
import numpy as np
import math
from assets.utils import split

# data model as follows:

# two dimensional array of dictionaries. Each dictionary represents a field and has these values
# flag indicating whether it is bt or refuge
# carrrying capacity
# population count of each of the 6 subpopulations

# initializer now fixes equal number of individuals across each field with no rr individuals
# initial size of the population is assumed to be the carrying capacity


class PopulationInitializer:

    # init_freq is f_0
    # capacity: two dimensional array of integers
    # bt_field_layout: two dimensional array of booleans
    # stochastic: boolean indicating if the model is stochastic
    def __init__(self, init_freq, capacity, bt_field_layout, stochastic):
        np_capacity = np.array(capacity)
        np_bt_field_layout = np.array(bt_field_layout)
        if init_freq > .25:
            raise ValueError('initial frequency cannot be greater than 0.25')
        elif len(np_capacity.shape) != 2:
            raise ValueError('capacity must be a 2 dimensional numpy array')
        elif len(np_capacity.shape) != 2:
            raise ValueError('bt_field_layourt must be a 2 dimensional numpy array')
        elif np_capacity.shape != np_bt_field_layout.shape:
            raise ValueError('dimensions of capacity and bt_field_layout do not agree')
        self.init_freq = init_freq
        self.capacity = np_capacity
        self.bt_field_layout = np_bt_field_layout
        self.stochastic = stochastic

    def get_init_freq(self):
        return self.init_freq

    def init(self):
        if self.stochastic:
            return self.__init_stochastic__()
        else:
            return self.__init_deterministic__()

    # WARNING: since we can't have fractional populations some stochasticity creeps in as far as
    # where we place remainder individuals.
    def __init_stochastic__(self):
        fields = [[None] * self.capacity.shape[1]] * self.capacity.shape[0]
        total_capacity = sum(self.capacity.flatten())
        total_rs = math.floor(2 * total_capacity * self.init_freq)
        rs_remainder = total_rs
        for i in range(self.capacity.shape[0]):
            for j in range(self.capacity.shape[1]):
                rs = math.floor(total_rs * self.capacity[i, j] / total_capacity)
                rs_remainder -= rs
                rs_m, rs_f = split(rs)
                ss_m, ss_f = split(self.capacity[i, j] - rs)
                fields[i][j] = {
                    "bt": self.bt_field_layout[i, j],
                    "capacity": self.capacity[i, j],
                    "subpop": {
                        "rr_m": 0,
                        "rr_f": 0,
                        "rs_m": rs_m,
                        "rs_f": rs_f,
                        "ss_m": ss_m,
                        "ss_f": ss_f
                    }
                }
        # now add remainder
        for i in range(int(rs_remainder)):
            row = np.random.randint(self.capacity.shape[0])
            col = np.random.randint(self.capacity.shape[1])
            if np.random.rand() < .5:
                fields[row][col]['subpop']['rs_m'] += 1
                fields[row][col]['subpop']['ss_m'] -= 1
            else:
                fields[row][col]['subpop']['rs_f'] += 1
                fields[row][col]['subpop']['ss_f'] -= 1
        return fields

    def __init_deterministic__(self):
        fields = [[None] * self.capacity.shape[1]] * self.capacity.shape[0]
        total_capacity = sum(self.capacity.flatten())
        total_rs = 2 * total_capacity * self.init_freq
        for i in range(self.capacity.shape[0]):
            for j in range(self.capacity.shape[1]):
                rs = total_rs * self.capacity[i, j] / total_capacity
                ss = self.capacity[i, j] - rs
                fields[i][j] = {
                    "bt": self.bt_field_layout[i, j],
                    "capacity": self.capacity[i, j],
                    "subpop": {
                        "rr_m": 0,
                        "rr_f": 0,
                        "rs_m": rs / 2,
                        "rs_f": rs / 2,
                        "ss_m": ss / 2,
                        "ss_f": ss / 2
                    }
                }
        return fields
