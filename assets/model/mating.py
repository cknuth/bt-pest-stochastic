from __future__ import division
from assets.utils import *


class Matchmaker:

    reproduction_matrix = np.array([
        [ 1.  ,  0.5 ,  0.  ,  0.5 ,  0.25,  0.  ,  0.  ,  0.  ,  0.  ],
        [ 0.  ,  0.5 ,  1.  ,  0.5 ,  0.5 ,  0.5 ,  1.  ,  0.5 ,  0.  ],
        [ 0.  ,  0.  ,  0.  ,  0.  ,  0.25,  0.5 ,  0.  ,  0.5 ,  1.  ]
    ])

    def __init__(self, fecundity, gendered, random_mating, random_reproduction, stochastic):
        if gendered and not random_reproduction and fecundity % 8 != 0:
            raise ValueError('With gendered mating and deterministic reproduction, fecundity must be divisible by 8')
        elif not gendered and not random_reproduction and fecundity % 4 != 0:
            raise ValueError('With nongendered mating and deterministic reproduction, fecundity must be divisible by 4')
        self.gendered = gendered
        self.fecundity = fecundity
        self.random_mating = random_mating
        self.random_reproduction = random_reproduction
        self.stochastic = stochastic

    def mate(self, fields):
        return map_by_field(self.__mate_within_field__, fields)

    def __mate_within_field__(self, field):
        if not self.gendered:
            pvals = self.__find_pvals_3_type__(field)
            if self.random_mating:
                num_pairs = np.random.multinomial(total(field) // 2, pvals)
            elif self.stochastic:
                total_pairs = total(field) // 2
                num_pairs = map(lambda pval: pval * total_pairs, pvals)
            else:
                total_pairs = total(field) / 2
                num_pairs = map(lambda pval: pval * total_pairs, pvals)
            if self.random_reproduction:
                # TODO: support random reproduction
                raise NotImplementedError('Currently we do not support random reproduction')
            else:
                offspring = self.__get_offspring_deterministic_3_type__(num_pairs)
            return {
                'bt': field['bt'],
                'capacity': field['capacity'],
                'subpop': offspring
            }
        elif self.gendered:
            # TODO: support gendered mating again
            raise NotImplementedError('Currently 6 type model is no longer supported')

    def __get_offspring_deterministic_3_type__(self, num_pairs):
        return self.__split_by_gender__(self.fecundity * np.dot(self.reproduction_matrix, num_pairs))

    def __split_by_gender__(self, nongendered_pop):
        if self.stochastic and not self.gendered:
            # with three type we don't care how gender is distributed
            rr_m, rr_f = split(round(nongendered_pop[0]))
            rs_m, rs_f = split(round(nongendered_pop[1]))
            ss_m, ss_f = split(round(nongendered_pop[2]))
        else:
            rr_m, rr_f = nongendered_pop[0] / 2, nongendered_pop[0] / 2
            rs_m, rs_f = nongendered_pop[1] / 2, nongendered_pop[1] / 2
            ss_m, ss_f = nongendered_pop[2] / 2, nongendered_pop[2] / 2
        return {
            'rr_m': rr_m,
            'rr_f': rr_f,
            'rs_m': rs_m,
            'rs_f': rs_f,
            'ss_m': ss_m,
            'ss_f': ss_f
        }

    @staticmethod
    def __find_pvals_3_type__(field):
        denom = total(field) ** 2
        pvals = [
            1 / denom,  # RR x RR
            1 / denom,  # RR x RS
            1 / denom,  # RR x SS
            1 / denom,  # RS x RR
            1 / denom,  # RS x RS
            1 / denom,  # RS x SS
            1 / denom,  # SS x RR
            1 / denom,  # SS x RS
            1 / denom   # SS x SS
        ]
        # create easily accessible structure for mating
        subpop3 = [
            rr(field),
            rs(field),
            ss(field)
        ]
        for type1 in [0, 1, 2]:
            for type2 in [0, 1, 2]:
                pvals[3*type1 + type2] *= subpop3[type1] * subpop3[type2]
        return pvals
