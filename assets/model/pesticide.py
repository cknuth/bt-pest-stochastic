import numpy as np
from assets.utils import *
import math


class Pesticide:

    # survival rate expected to look like:
    # { 'bt': {'rr': s1, 'rs': s2, 'ss': s3}, 'ref': {'rr': s4, 'rs': s5, 'ss':s6} }
    def __init__(self, survival_rate, rand, disallow_continuous):
        self.survival_rate = survival_rate
        self.rand = rand
        self.allow_continuous = not disallow_continuous

    def kill(self, fields):
        return map_by_field(self.__kill_within_field__, fields)

    def __kill_within_field__(self, field):
        field_type = 'bt' if field['bt'] else 'ref'
        result = {
            'bt': field['bt'],
            'capacity': field['capacity'],
            'subpop': {
                'rr_m': 0,
                'rr_f': 0,
                'rs_m': 0,
                'rs_f': 0,
                'ss_m': 0,
                'ss_f': 0
            }
        }
        if self.rand and not self.allow_continuous:
            for k, v in field['subpop'].items():
                result['subpop'][k] = np.random.binomial(v, self.survival_rate[field_type][k[:2]])
        if self.rand and self.allow_continuous:
            for k, v in field['subpop'].items():
                if self.survival_rate[field_type][k[:2]] == 1:
                    result['subpop'][k] = v
                elif self.survival_rate[field_type][k[:2]] == 0 or v <= 0:
                    result['subpop'][k] = 0
                else:
                    result['subpop'][k] = np.random.normal(v*self.survival_rate[field_type][k[:2]],
                                                       v*self.survival_rate[field_type][k[:2]]*(1 - self.survival_rate[field_type][k[:2]]),
                                                       1)[0]
        elif not self.rand and not self.allow_continuous:
            for k, v in field['subpop'].items():
                result['subpop'][k] = math.floor(v * self.survival_rate[field_type][k[:2]])
        elif not self.rand and self.allow_continuous:
            # deterministic, allow noninteger pest counts
            for k, v in field['subpop'].items():
                result['subpop'][k] = v * self.survival_rate[field_type][k[:2]]
        return result
