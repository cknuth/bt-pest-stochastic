from __future__ import division
from assets.utils import *
from math import ceil, floor


class DensityDependentSurvival:

    def __init__(self, gendered, rand, stochastic):
        self.rand = rand
        self.stochastic = stochastic
        self.gendered = gendered

    def kill(self, fields):
        return map_by_field(self.__kill_within_field__, fields)

    def __kill_within_field__(self, field):
        result = {
            'bt': field['bt'],
            'capacity': field['capacity'],
            'subpop': {
                'rr_m': 0,
                'rr_f': 0,
                'rs_m': 0,
                'rs_f': 0,
                'ss_m': 0,
                'ss_f': 0
            }
        }
        total_pests = total(field)
        if total_pests == 0:
            return result

        if self.rand:
            raise NotImplementedError('Currently there is no support for random density dependent killing')
        elif not self.rand and self.stochastic and self.gendered:
            for k, v in field['subpop'].items():
                result['subpop'][k] = (field['capacity'] * v) // total_pests
        elif not self.rand and self.stochastic and not self.gendered:
            result['subpop']['rr_m'], result['subpop']['rr_f'] = split(round((field['capacity'] * rr(field)) / total_pests))
            result['subpop']['rs_m'], result['subpop']['rs_f'] = split(round((field['capacity'] * rs(field)) / total_pests))
            result['subpop']['ss_m'], result['subpop']['ss_f'] = split(round((field['capacity'] * ss(field)) / total_pests))
        elif not self.rand and not self.stochastic:
            for k, v in field['subpop'].items():
                result['subpop'][k] = (field['capacity'] * v) / total_pests
        return result

