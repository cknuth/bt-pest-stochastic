import numpy as np
import math


class MigratorTwoField:

    def __init__(self, bt_migration, ref_migration, rand, stochastic):
        self.bt_migration = bt_migration
        self.ref_migration = ref_migration
        self.rand = rand
        self.stochastic = stochastic

    # WARNING: this is an in place operation to save computing time
    def migrate(self, fields):
        if self.rand:
            self.__migrate_rand__(fields)
        else:
            self.__migrate_deterministic__(fields)

    def __migrate_rand__(self, fields):
        emigrants = {}
        for key, value in fields[0][0]['subpop'].items():
            emigrants['bt_' + key] = np.random.binomial(value, self.bt_migration)
        for key, value in fields[0][1]['subpop'].items():
            emigrants['ref_' + key] = np.random.binomial(value, self.ref_migration)
        for k in fields[0][0]['subpop'].keys():
            fields[0][0]['subpop'][k] += emigrants['ref_' + k] - emigrants['bt_' + k]
        for k in fields[0][1]['subpop'].keys():
            fields[0][1]['subpop'][k] += emigrants['bt_' + k] - emigrants['ref_' + k]

    def __migrate_deterministic__(self, fields):
        emigrants = {}
        if self.stochastic:
            for key, value in fields[0][0]['subpop'].items():
                emigrants['bt_' + key] = math.ceil(value * self.bt_migration)
            for key, value in fields[0][1]['subpop'].items():
                emigrants['ref_' + key] = math.ceil(value * self.ref_migration)
        else:
            # pests can be nonintegers
            for key, value in fields[0][0]['subpop'].items():
                emigrants['bt_' + key] = value * self.bt_migration
            for key, value in fields[0][1]['subpop'].items():
                emigrants['ref_' + key] = value * self.ref_migration
        for k in fields[0][0]['subpop'].keys():
            fields[0][0]['subpop'][k] += emigrants['ref_' + k] - emigrants['bt_' + k]
        for k in fields[0][1]['subpop'].keys():
            fields[0][1]['subpop'][k] += emigrants['bt_' + k] - emigrants['ref_' + k]
