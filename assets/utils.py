from __future__ import division
import numpy as np


def rr(field):
    return field['subpop']['rr_f'] + field['subpop']['rr_m']


def rs(field):
    return field['subpop']['rs_f'] + field['subpop']['rs_m']


def ss(field):
    return field['subpop']['ss_f'] + field['subpop']['ss_m']


def types(field):
    return rr(field), rs(field), ss(field)


def genotypes_all_fields(fields):
    type_list = map(lambda f: (rr(f), rs(f), ss(f)), [field for sublist in fields for field in sublist])
    return reduce(lambda f1, f2: (f1[0] + f2[0], f1[1] + f2[1], f1[2] + f2[2]), type_list)


def map_by_field(converter, fields):
    result = [[None] * len(fields[0])] * len(fields)
    for i in range(len(fields)):
        for j in range(len(fields[0])):
            result[i][j] = converter(fields[i][j])
    return result


def total(field):
    return sum(types(field))


def grand_total(fields):
    return reduce(lambda a, b: a + b, [total(field) for sublist in fields for field in sublist])


def calc_freq(fields):
    totals = genotypes_all_fields(fields)
    if totals == (0, 0, 0):
        return np.nan
    else:
        return (2 * totals[0] + totals[1]) / (2 * totals[0] + 2 * totals[1] + 2 * totals[2])


def equals_fields(fields1, fields2):
    if len(fields1) != len(fields2) or len(fields1[0]) != len(fields2[0]):
        return False
    for i in range(len(fields1)):
        for j in range(len(fields1[0])):
            if not equals_field(fields1[i][j], fields2[i][j]):
                return False
    return True


def equals_field(field1, field2):
    if field1['bt'] != field2['bt'] or field1['capacity'] != field2['capacity']:
        return False
    if not equals_subpop(field1['subpop'], field2['subpop']):
        return False
    return True


def equals_subpop(subpop1, subpop2):
    for k in subpop1.keys():
        if subpop1[k] != subpop2[k]:
            return False
    return True


# split an integer into two integers such that a + b = num
def split(num):
    one, two = 0, 0
    if num % 2 == 1:
        if np.random.rand() < .5:
            one = 1
        else:
            two = 1
    return num // 2 + one, num // 2 + two
