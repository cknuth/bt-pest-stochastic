import sys


# parse args or use defaults
def parse_args():

    print "loading configuration"
    # SCRIPT ARGS
    config_module = None
    batch = False
    graph = True
    threshold = .95
    num_sim = 1
    i = 0
    while i < len(sys.argv):
        if i == 1:
            # this is our config file
            config_module = sys.argv[i]
        elif sys.argv[i] == '-n':  # the number of simulations to run
            i += 1
            num_sim = int(sys.argv[i])
        elif sys.argv[i] == '-b':  # will write a file of the data from across the runs
            batch = True
        elif sys.argv[i] == '--no-graph':  # will not graph while in batch mode
            graph = False
        elif sys.argv[i] == '--threshold':
            i += 1
            threshold = float(sys.argv[i])
        elif sys.argv[i] == 'bt_pest_model.py':
            pass
        elif 'bt_pest_model.py' in sys.argv[i] and sys.argv[i] != 'bt_pest_model.py':
            # We're not in right directory
            raise Exception('Must execute script from the same directory')
        else:
            raise Exception('Unrecognized command line option: ' + sys.argv[i])
        i += 1

    config = __import__(config_module, globals(), locals(), ['init_freqs'])

    return config.init_freqs, config.fecundity, config.capacity, config.bt_field_layout, config.migration_scheme, \
        config.migration_args, config.survival_rate, config.gendered, config.rand, config.stochastic, \
        batch, graph, threshold, num_sim, config_module

