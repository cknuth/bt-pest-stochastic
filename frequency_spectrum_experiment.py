from __future__ import division
from pymongo import MongoClient
import numpy as np
import matplotlib.pyplot as plt
from bson.objectid import ObjectId
from bt_pest_model import run_test
from copy import deepcopy

u_double_prime = 18
V_01_prime = 27 / 50
V_10_prime = 6
k1 = 2.07
k2 = 1


# vary over frequency for two different survival probabilities
def run_simulations():
    client = MongoClient()
    db = client['bt-pest-model']
    confs = db.configurations.find({'collection': 'FREQUENCY_TEST_1'})
    cc = db.conf_collection.find_one({'collection_name': 'FREQUENCY_TEST_1'})
    c_run = cc['collection_runs'] + 1
    params = db.run_params.find_one({'_id': ObjectId('5d5c8bef113a0e72ecdab18d')})

    for conf in confs:
        run_test(db, conf, params, c_run)

    db.conf_collection.update_one(
        {'_id': cc['_id']},
        {'$set': {'collection_runs': c_run}}
    )


def graph_data(collection_run):
    client = MongoClient()
    db = client['bt-pest-model']
    confs = db.run_instance.find({'model_config.collection': 'FREQUENCY_TEST_1', 'collection_run': collection_run})
    t_ode = [[], [], [], [], [], []]
    t_sde = [[], [], [], []]
    t_sim = [[], [], [], [], [], []]
    for c in confs:
        if abs(c['model_config']['survival_rate']['bt']['rs'] - 0.1) < 0.05 and \
                (c['model_config']['rand']['pesticide'] and not c['model_config']['rand']['mating']):
            t_ode[0].append([c['model_config']['frequency'], c['ode_analysis']['saturation_exp']])
            t_sim[0].append([c['model_config']['frequency'], c['analysis_by_frequency'][0]['analysis']['saturation_exp']])
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde[0].append([c['model_config']['frequency'], c['sde_analysis']['saturation_exp']])
        elif abs(c['model_config']['survival_rate']['bt']['rs'] - 0.1) < 0.05 and \
                (c['model_config']['rand']['mating'] and not c['model_config']['rand']['pesticide']):
            t_ode[1].append([c['model_config']['frequency'], c['ode_analysis']['saturation_exp']])
            t_sim[1].append([c['model_config']['frequency'], c['analysis_by_frequency'][0]['analysis']['saturation_exp']])
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde[1].append([c['model_config']['frequency'], c['sde_analysis']['saturation_exp']])
        elif abs(c['model_config']['survival_rate']['bt']['rs'] - 0.1) < 0.05 and \
                (c['model_config']['rand']['mating'] and c['model_config']['rand']['pesticide']):
            t_ode[2].append([c['model_config']['frequency'], c['ode_analysis']['saturation_exp']])
            t_sim[2].append([c['model_config']['frequency'], c['analysis_by_frequency'][0]['analysis']['saturation_exp']])
        elif abs(c['model_config']['survival_rate']['bt']['rs'] - 0.2) < 0.05 and \
                (c['model_config']['rand']['pesticide'] and not c['model_config']['rand']['mating']):
            t_ode[3].append([c['model_config']['frequency'], c['ode_analysis']['saturation_exp']])
            t_sim[3].append([c['model_config']['frequency'], c['analysis_by_frequency'][0]['analysis']['saturation_exp']])
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde[2].append([c['model_config']['frequency'], c['sde_analysis']['saturation_exp']])
        elif abs(c['model_config']['survival_rate']['bt']['rs'] - 0.2) < 0.05 and \
                (c['model_config']['rand']['mating'] and not c['model_config']['rand']['pesticide']):
            t_ode[4].append([c['model_config']['frequency'], c['ode_analysis']['saturation_exp']])
            t_sim[4].append([c['model_config']['frequency'], c['analysis_by_frequency'][0]['analysis']['saturation_exp']])
            if c['sde_analysis']['saturation_exp'] > 0:
                t_sde[3].append([c['model_config']['frequency'], c['sde_analysis']['saturation_exp']])
        elif abs(c['model_config']['survival_rate']['bt']['rs'] - 0.2) < 0.05 and \
                (c['model_config']['rand']['mating'] and c['model_config']['rand']['pesticide']):
            t_ode[5].append([c['model_config']['frequency'], c['ode_analysis']['saturation_exp']])
            t_sim[5].append([c['model_config']['frequency'], c['analysis_by_frequency'][0]['analysis']['saturation_exp']])
        else:
            print('unexpected value')
    t_ode = np.array(t_ode)
    t_sde = np.array(t_sde)
    t_sim = np.array(t_sim)

    a_01 = np.array([[f, analytic_01(f)] for f in np.linspace(0.0001, 0.005, 50)])
    a_10 = np.array([[f, analytic_10(f)] for f in np.linspace(0.0001, 0.005, 50)])
    a_11 = np.array([[f, analytic_01(f) + analytic_10(f)] for f in np.linspace(0.0001, 0.005, 50)])

    for i in range(6):
        plt.figure(i+1)
        plt.plot(t_ode[i,:,0], t_ode[i,:,1])
        plt.plot(t_sim[i,:,0], t_sim[i,:,1], 'x')
        if i == 0:
            plt.plot(a_01[:,0], a_01[:,1])
            plt.plot(t_sde[i if i < 2 else i-1,:,0], t_sde[i if i < 2 else i-1,:,1], 'x')
            plt.legend(('t_ode', 't_sim', 't_analytic', 't_sde'))
        elif i == 1:
            plt.plot(a_10[:,0], a_10[:,1])
            plt.plot(t_sde[i if i < 2 else i-1,:,0], t_sde[i if i < 2 else i-1,:,1], 'x')
            plt.legend(('t_ode', 't_sim', 't_analytic', 't_sde'))
        elif i == 2:
            plt.plot(a_11[:,0], a_11[:,1])
            plt.legend(('t_ode', 't_sim', 't_analytic'))
        elif i != 5:
            plt.plot(t_sde[i if i < 2 else i-1,:,0], t_sde[i if i < 2 else i-1,:,1], 'x')
            plt.legend(('t_ode', 't_sim', 't_sde'))
        else:
            plt.legend(('t_ode', 't_sim'))
        if i == 0 or i == 3:
            model = '01'
        elif i == 1 or i == 4:
            model = '10'
        else:
            model = '11'
        if i <= 2:
            w_rs = '0.1'
        else:
            w_rs = '0.2'
        plt.title('w_rs = ' + w_rs + ', (' + model + ') model')
        plt.xlabel('intial frequency')
        plt.ylabel('t')
        if i == 0 or i == 1 or i == 2:
            plt.ylim((0, 200))
        plt.show()


def analytic_01(f):
    return 100 * (np.sqrt(2 / (u_double_prime * V_01_prime))*k1 - k2/V_01_prime*100*f)


def analytic_10(f):
    return 100 * (np.sqrt(2 / (u_double_prime * V_10_prime))*k1 - k2/V_10_prime*100*f)


def setup_data():
    #print('data already set up')
    #return

    client = MongoClient()
    db = client['bt-pest-model']
    mc = db.configurations.find_one({'_id': ObjectId('5aef2699ec925e0037b3de24')})

    mc.pop('_id', None)
    mc['collection'] = 'FREQUENCY_TEST_1'
    mc.pop('notes', None)

    c_ids = []

    N = 10000
    frequencies = np.linspace(0.0001, 0.005, 50)
    for w_rs in [0.1, 0.2]:
        for model in ['10', '01', '11']:
            for freq in frequencies:
                c = deepcopy(mc)
                c['survival_rate']['bt']['rs'] = w_rs
                c['survival_rate']['ref']['rs'] = w_rs
                c['capacity'][0][0] = N
                c['frequency'] = freq
                if model == '01':
                    c['rand']['mating'] = False
                    c['rand']['pesticide'] = True
                elif model == '10':
                    c['rand']['mating'] = True
                    c['rand']['pesticide'] = False
                elif model == '11':
                    c['rand']['mating'] = True
                    c['rand']['pesticide'] = True
                else:
                    raise Exception('unexpected model')
                c_ids.append(db.configurations.insert_one(c).inserted_id)

    db.conf_collection.insert_one(
        {
            'collection_name': 'FREQUENCY_TEST_1',
            'conf_ids': c_ids,
            'collection_runs': 0
        }
    )
