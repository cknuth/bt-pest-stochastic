syms s1 s2 s3 F f
u = ( (s2-s1)*f + (s3-3*s2+2*s1)*f^2 - (s3-2*s2+s1)*f^3 ) / (s1 + 2*(s2-s1)*f + (s3-2*s2+s1)*f^2);

V_01 = F / 4 * (s2*(1-s2)*f*(1-f) + 2*s3*(1-s3)*f^2);

V_10 = F / 4 * f * (1-f);

disp('V_01 prime at f=0, w_rs=0.1')
simplify(subs(diff(V_01, f), [f s1 s2 s3 F], [0 0.1 0.1 1 24]))

disp('V_10 prime at f=0, w_rs=0.1')
simplify(subs(diff(V_10, f), [f s1 s2 s3 F], [0 0.1 0.1 1 24]))

disp('u double prime at f=0, w_rs=0.1')
simplify(subs(diff(diff(u,f),f), [f s1 s2 s3 F], [0 0.1 0.1 1 24]))

