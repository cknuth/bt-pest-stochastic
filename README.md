# bt-pest-model

This documentation should be considered a rough sketch and in progress.

## Prereqs

* python 2.7
* mongodb 3.6
* [Optional] compass (to use in examining the running mongo instance)

## Setting up and using the mongo db

This project relies on a running mongo server instance on localhost:27017.

The DB should be brought online using the dump under `/dump` like so:

```
mongod.exe
```

then in a separate terminal from the project directory:

```
mongorestore.exe --db bt-pest-model --drop
```

Note drop will remove the existing collection. By default, `mongorestore` will not update existing records, so we use drop to ensure any updates to existing documents are performed.

After runs have been added to the database, ensure that it is stored in the repository by running the following in the project directory:

```
mongodump.exe --db bt-pest-model
```

## Running

When the mongo database is up, run the simulation using

```
python -u bt_pest_model.py <configuration id> <run param id>
```

For example, `python -u bt_pest_model.py 5aef2699ec925e0037b3de24 5aef5ca8ec925e01549f3c6c`.

Running with `-u` turns off buffered printing, meaning you get real time updates from the script.

The configuration and run parameters are stored in the mongo database. See the documents to see different options for parameters or create a new document with new parameters.

The raw results are written to the collection `run_results`. These results are analyzed and the analysis is stored in the collection `run_instance`.

## Testing

Tests are located under `/test` and use the [unittest](https://docs.python.org/2/library/unittest.html) framework.