from assets.file_io import *
import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

# BIG WARNING: This has been specially coded for frequency 0.001 TODO: fix that
# Another warning: this has not been rewritten for mongo DB interaction


def main():
    csv_files = []
    i = 0
    thresh = .5
    demographics = False
    while i < len(sys.argv):
        if sys.argv[i] == 'histogram.py':
            pass
        elif os.path.basename(sys.argv[i]) == 'frequency.csv':
            csv_files.append(sys.argv[i])
        elif sys.argv[i] == '--demographics' or sys.argv[i] == '-d':
            demographics = True
        elif sys.argv[i] == '-t':
            i += 1
            thresh = float(sys.argv[i])
        else:
            print sys.argv[i] + ' is not a output csv file'
        i += 1
    for csv_file in csv_files:
        print 'analyzing file ' + csv_file + ' '
        runs, params = read_frequency_file(csv_file)
        if not params:
            raise Exception('file ' + csv_file + ' could not be read')
        if demographics:
            saturated, extinct = gens_by_saturated_and_extinct(runs)
            if not saturated and not extinct:
                raise Exception('error occurred while computing demographics of ' + csv_file)
            output_file = determine_output_path(params, 'histogram_saturation', 'png', 1)
            ensure_path_exists(os.path.dirname(output_file))
            write_hist_double(params, saturated, extinct, output_file)
        else:
            hitting_times = hitting_time_dist(runs, thresh)
            output_file = determine_output_path(params, 'hitting_time_' + str(thresh), 'png', 1)
            ensure_path_exists(os.path.dirname(output_file))
            write_hist(params, hitting_times, output_file)


def hitting_time_dist(runs, thresh):
    hitting_times = []
    for run in runs:
        l = [i+1 for i, v in enumerate(run) if v > thresh]
        if l:
            # TODO: remove the ten, calculate for each init freq and let it be a dictionary
            hitting_times.append(l[0] - 11)
            # hitting_times.append(l[0])
    return hitting_times


def gens_by_saturated_and_extinct(runs):
    saturated = []
    extinct = []
    for run in runs:
        gen = len(run)
        if run[gen - 1] > .95:
            saturated.append(gen)
        else:
            extinct.append(gen)
    return saturated, extinct


def write_hist(params, data, output_file):
    # chop off top 1 percent of the runs to eliminate outliers
    a, l, b = stats.gamma.fit(data, floc=0)
    m = str(np.mean(data))
    v = str(np.var(data))
    print 'mean: ' + m
    print 'var: ' + v
    print 'alpha: ' + str(a)
    print 'loc: ' + str(l)
    print 'beta: ' + str(b)
    data.sort()
    data = data[:int(floor(.99 * len(data)))]

    smallest = min(data)
    largest = max(data)
    plt.hist(data, largest - smallest, normed=1, color=['seagreen'])
    domain = np.linspace(smallest, largest)
    plt.plot(domain, stats.gamma.pdf(domain, a, loc=0, scale=b), 'midnightblue', linewidth=2.0)
    plt.xlabel("Generations")
    plt.ylabel("Frequency")
    plt.show()


def write_hist_double(params, saturated, extinct, output_file):
    # chop off top 1 percent of the runs to eliminate outliers
    saturated.sort()
    saturated = saturated[:int(floor(.99 * len(saturated)))]
    extinct.sort()
    extinct = extinct[:int(floor(.99 * len(extinct)))]

    smallest = min(min(saturated), min(extinct))
    largest = max(max(saturated), max(saturated))
    plt.hist([extinct, saturated], largest - smallest, normed=1, color=['red', 'blue'], label=['extinct', 'saturated'])
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
