

Configuration: config_files.three_type.many_deterministic
Initial frequency: 0.0005

There were 1 runs that ended in saturation. 
	Max gen: 231
	Min gen: 231
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 231.0 generations
extinction: -1 generations