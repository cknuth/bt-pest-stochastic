

Configuration: config_files.three_type.many_deterministic
Initial frequency: 0.003

There were 1 runs that ended in saturation. 
	Max gen: 44
	Min gen: 44
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 44.0 generations
extinction: -1 generations