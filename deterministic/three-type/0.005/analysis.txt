

Configuration: config_files.three_type.many_deterministic
Initial frequency: 0.005

There were 1 runs that ended in saturation. 
	Max gen: 29
	Min gen: 29
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 29.0 generations
extinction: -1 generations