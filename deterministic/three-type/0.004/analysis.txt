

Configuration: config_files.three_type.many_deterministic
Initial frequency: 0.004

There were 1 runs that ended in saturation. 
	Max gen: 35
	Min gen: 35
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 35.0 generations
extinction: -1 generations