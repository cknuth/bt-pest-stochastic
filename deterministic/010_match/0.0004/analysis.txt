

Configuration: config_files.three_type.deterministic_010_match
Initial frequency: 0.0004

There were 1 runs that ended in saturation. 
	Max gen: 287
	Min gen: 287
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 287.0 generations
extinction: -1 generations