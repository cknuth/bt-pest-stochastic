

Configuration: config_files.three_type.deterministic_010_match
Initial frequency: 0.0001

There were 1 runs that ended in saturation. 
	Max gen: 1122
	Min gen: 1122
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 1122.0 generations
extinction: -1 generations