

Configuration: config_files.three_type.deterministic_010_match
Initial frequency: 0.006

There were 1 runs that ended in saturation. 
	Max gen: 25
	Min gen: 25
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 25.0 generations
extinction: -1 generations