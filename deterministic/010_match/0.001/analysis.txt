

Configuration: config_files.three_type.deterministic_010_match
Initial frequency: 0.001

There were 1 runs that ended in saturation. 
	Max gen: 119
	Min gen: 119
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 119.0 generations
extinction: -1 generations