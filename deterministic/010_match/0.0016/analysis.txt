

Configuration: config_files.three_type.deterministic_010_match
Initial frequency: 0.0016

There were 1 runs that ended in saturation. 
	Max gen: 77
	Min gen: 77
No run resulted in extinction

Expectations:
simple threshold: 0.0 generations
saturation: 77.0 generations
extinction: -1 generations