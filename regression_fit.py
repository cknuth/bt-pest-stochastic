from __future__ import division
import math

X_N_INCREMENTED = [8000, 9000, 10000, 11000, 12000, 13000, 14000, 15000, 16000, 17000, 18000, 19000, 20000, 21000, 22000, 23000,
     24000, 25000, 26000, 27000, 28000, 29000, 30000, 31000, 32000, 33000, 34000, 35000, 36000, 37000, 38000, 39000,
     40000, 41000, 42000, 43000, 44000, 45000, 46000, 47000, 48000, 49000, 50000, 51000, 52000, 53000, 54000, 55000,
     56000, 57000, 58000, 59000, 60000, 61000, 62000, 63000, 64000, 65000, 66000, 67000, 68000, 69000, 70000, 71000,
     72000, 73000, 74000, 75000, 76000, 77000, 78000, 79000, 80000, 81000, 82000, 83000, 84000, 85000, 86000, 87000,
     88000, 89000, 90000, 91000, 92000, 93000, 94000, 95000, 96000, 97000, 98000, 99000, 100000]
Y_N_INCREMENTED = [0.0048000000000000004, 0.0046, 0.0044, 0.004200000000000001, 0.004, 0.0038, 0.0037, 0.0037, 0.0038, 0.0035, 0.0037,
     0.0033, 0.0037, 0.0031000000000000003, 0.0031000000000000003, 0.0032, 0.0034000000000000002, 0.0032, 0.003, 0.0028,
     0.0025, 0.0026000000000000003, 0.0024000000000000002, 0.0026000000000000003, 0.0025, 0.0024000000000000002,
     0.0024000000000000002, 0.0027, 0.0021000000000000003, 0.0026000000000000003, 0.0024000000000000002,
     0.0021000000000000003, 0.0024000000000000002, 0.0023, 0.0023, 0.0023, 0.0023, 0.0022, 0.0024000000000000002, 0.002,
     0.0021000000000000003, 0.002, 0.0019, 0.002, 0.002, 0.0021000000000000003, 0.0021000000000000003, 0.0019, 0.0022,
     0.002, 0.0018000000000000002, 0.002, 0.0022, 0.0018000000000000002, 0.0019, 0.002, 0.0021000000000000003, 0.0019,
     0.0019, 0.0018000000000000002, 0.0016, 0.0019, 0.0016, 0.0018000000000000002, 0.0018000000000000002,
     0.0017000000000000001, 0.0015, 0.0017000000000000001, 0.0017000000000000001, 0.0017000000000000001,
     0.0018000000000000002, 0.0014, 0.0016, 0.0017000000000000001, 0.0016, 0.0016, 0.0014,
     0.0017000000000000001, 0.0015, 0.0015, 0.0016, 0.0015, 0.0018000000000000002,
     0.0017000000000000001, 0.0016, 0.0017000000000000001, 0.0013000000000000002, 0.0015, 0.0016, 0.0015, 0.0015,
     0.0016, 0.0015]


def find_fit(X=X_N_INCREMENTED, Y=Y_N_INCREMENTED, initial=0.5, initial_delta=0.1, delta_min=1e-12):
    point = initial
    delta = initial_delta
    while delta >= delta_min:
        sample = three_point_sample(X, Y, point, delta)
        if sample[0] < sample[1] < sample[2]:
            # min is to the left
            point -= delta
        elif sample[0] >= sample[1] and sample[1] <= sample[2]:
            # min is near the sample midpoint
            delta *= 0.1
            print "reducing delta to " + str(delta)
        elif sample[0] > sample[1] > sample[2]:
            # min is to the right
            point += delta
        elif sample[0] <= sample[1] and sample[1] >= sample[2]:
            # min is either to left or to right
            print "Warning: this function assumes one minimum, found divergence"
            print sample
            return point, delta
        else:
            print "reexamine your conditions!"
            break
    return point, delta


def three_point_sample(X, Y, sample_point, delta):
    return (least_squares_fit(X, Y, sample_point - delta),
            least_squares_fit(X, Y, sample_point),
            least_squares_fit(X, Y, sample_point + delta))


def least_squares_fit(X, Y, point):
    fitted_function_output = [one_over_square_root(x, point) for x in X]
    square_differences = [(y - o)**2 for y, o in zip(Y, fitted_function_output)]
    return reduce(lambda d1, d2: d1 + d2, square_differences, 0)


def one_over_square_root(x, c):
    return math.sqrt( c / x )
