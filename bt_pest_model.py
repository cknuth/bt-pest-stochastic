from assets.file_io import *
from assets.model.density_dependent_survival import DensityDependentSurvival
from assets.model.mating import Matchmaker
from assets.model.pesticide import Pesticide
from assets.model.population_initializer import PopulationInitializer
from assets.utils import *
from assets.model.migration.migration_scheme import *
from assets.model.migration.migrator_one_field import MigratorOneField
from assets.model.migration.migrator_two_field import MigratorTwoField
from pymongo import MongoClient
from bson.objectid import ObjectId
from analyzer import analyze_and_write_all_runs, analyze_all_runs
from ode import run_simulations_mongo
import datetime
import time
import copy


def main():
    client = MongoClient()
    db = client['bt-pest-model']
    print "loading configuration"

    model_config = db.configurations.find_one({'_id': ObjectId(sys.argv[1])})
    run_config = db.run_params.find_one({'_id': ObjectId(sys.argv[2])})
    run_test(db, model_config, run_config)


def run_test(db, model_config, run_config, collection_run=None):
    if model_config is None:
        print 'could not find model config'
        return
    elif run_config is None:
        print 'could not find run parameters'
        return

    # first run the deterministic version in order to compare (but don't insert)
    d_init_freq, freq, det_run_time, det_saturated, time_out = run_deterministic_simulation(model_config, run_config)

    ps, mig, mat, pcide, d = generate_classes([model_config['frequency']], model_config['fecundity'],
                                              model_config['capacity'], model_config['bt_field_layout'],
                                              model_config['migration_scheme'], {}, model_config['survival_rate'],
                                              model_config['gendered'], model_config['rand'],
                                              not model_config['allows_continuous'])

    run_instance_id = db.run_instance.insert_one(
        {
            'model_config': model_config,
            'collection_run': collection_run,
            'run_config': run_config,
            'deterministic_run': {
                'init_freq': d_init_freq,
                'frequencies': freq,
                'saturated': det_saturated,
                'det_run_time': det_run_time,
                'time_out': time_out
            },
            'run_at': datetime.datetime.utcnow()
        }
    ).inserted_id

    frequencies_list = []
    initial_frequency_list = []
    for p in ps:
        populations, frequencies, time_out = run_all_simulations(p.get_init_freq(), run_config['number_of_sims'],
                                                       p, mig, mat, pcide, d, run_config['threshold'])
        initial_frequency_list.append(p.get_init_freq())
        frequencies_list.append(frequencies)
        if time_out:
            db.run_instance.update_one(
                {'_id': run_instance_id},
                {'$set': {'time_out': True}}
            )
            break


    #run_simulations_mongo(db, run_instance_id, model_config, run_config)
    analyze_and_write_all_runs(db, run_instance_id, frequencies_list, initial_frequency_list, det_run_time, det_saturated)


def run_deterministic_simulation(model_config, run_config):
    d_mc = copy.deepcopy(model_config)
    for k in d_mc['rand'].keys():
        d_mc['rand'][k] = False


    d_ps, d_mig, d_mat, d_pcide, d_d = generate_classes([d_mc['frequency']], d_mc['fecundity'],
                                              d_mc['capacity'], d_mc['bt_field_layout'],
                                              d_mc['migration_scheme'], {}, d_mc['survival_rate'],
                                              d_mc['gendered'], d_mc['rand'],
                                              not d_mc['allows_continuous'])
    d_run_instance = {
            'model_config': d_mc,
            'run_config': run_config,
            'time_out': False
        }
    frequencies_list = []
    initial_frequency_list = []
    time_out = False
    for d_p in d_ps:
        d_num_sim = 1
        populations, frequencies, time_out = run_all_simulations(d_p.get_init_freq(), d_num_sim,
                                                       d_p, d_mig, d_mat, d_pcide, d_d, run_config['threshold'])
        initial_frequency_list.append(d_p.get_init_freq())
        frequencies_list.append(frequencies)
        if time_out:
            d_run_instance['time_out'] = True
            break

    analysis_by_freq = analyze_all_runs(frequencies_list, initial_frequency_list)
    saturated = analysis_by_freq[0]['analysis']['saturation_exp'] == 0
    det_run_time = len(frequencies_list[0])
    # if analysis_by_freq[0]['analysis']['saturation_exp'] == 0:
    #     det_run_time = -1  # run_time of 1 means it did not saturate
    # else:
    #     det_run_time = analysis_by_freq[0]['analysis']['saturation_exp']
    return initial_frequency_list[0], frequencies_list[0], det_run_time, saturated, time_out


def run_all_simulations(init_freq, num_sim, p, mig, mat, pcide, d, threshold):
    print "Running " + str(num_sim) + " simulations with initial frequency " + str(init_freq)
    populations = []
    frequencies = []
    start_time = time.time()
    for i in range(num_sim):
        if time.time() - start_time > 30*60*1:
            print 'time out'
            return [], [], True
        if i % 500 is 0:
            sys.stdout.write('.'),
        population, freq, iterate_out = run_simulation(p, mig, mat, pcide, d, threshold)
        if not iterate_out:
            populations.append(population)
            frequencies.append(freq)
    print 'end'
    return populations, frequencies, False


def graph_batch_runs(runs, init_freq, config_module, num_sim):
    if num_sim < 5:
        graph_runs(runs[0:num_sim], init_freq, config_module, num_sim)
    else:
        graph_runs(runs[0:5], init_freq, config_module, 5)
    if num_sim >= 10:
        graph_runs(runs[0:10], init_freq, config_module, 10)
    if num_sim >= 20:
        graph_runs(runs[0:20], init_freq, config_module, 20)


# individual is represented by a 2 element array
# the first element is the type, 0 = SS, 1 = RS, 2 = RR
# the second element is the field, 0 = Bt, 1 = Ref
def generate_classes(init_freqs, fecundity, capacity, bt_field_layout, migration_scheme,
                     migration_args, survival_rate, gendered, rand, stochastic):
    ps = [PopulationInitializer(init_freq, capacity, bt_field_layout, stochastic) for init_freq in init_freqs]
    if migration_scheme == ONE_FIELD:
        mig = MigratorOneField()
    elif migration_scheme == TWO_FIELD:
        mig = MigratorTwoField(migration_args['bt_emigration_rate'], migration_args['ref_emigration_rate'],
                               rand['migration'], stochastic)
    else:
        raise ValueError('Unrecognized migration scheme: ' + migration_scheme)
    mat = Matchmaker(fecundity, gendered, rand['mating'], rand['reproduction'], stochastic)
    pcide = Pesticide(survival_rate, rand['pesticide'], stochastic)
    d = DensityDependentSurvival(gendered, rand['competition'], stochastic)
    return ps, mig, mat, pcide, d


def run_simulation(p, mig, mat, pcide, d, threshold):
    # grab initial population
    fields = p.init()
    ss_count, rs_count, rr_count = genotypes_all_fields(fields)
    total = ss_count + rs_count + rr_count
    population = [[ss_count, rs_count, rr_count]]
    freq = [calc_freq(fields)]

    generation = 0
    # print "starting simulation: ",
    # Break out by calculating frequency of resistant gene below
    while generation <= 100000:
        generation += 1
        #print str(generation)
        mig.migrate(fields)
        #print str(fields)
        fields = mat.mate(fields)
        #print str(fields)
        fields = pcide.kill(fields)
        #print str(fields)
        fields = d.kill(fields)
        #print str(fields)

        ss_count, rs_count, rr_count = genotypes_all_fields(fields)
        population = np.concatenate((population, [[ss_count, rs_count, rr_count]]))
        freq.append(calc_freq(fields))
        if freq[generation] >= threshold or freq[generation] <= 2. / float(4*total) or \
                (ss_count == 0 and rs_count == 0 and rr_count == 0):
            break

    if generation >= 100000:
        iterate_out = True
    else:
        iterate_out = False

    # print "done after " + str(generation) + " generations"
    return population, freq, iterate_out


if __name__ == '__main__':
    main()
