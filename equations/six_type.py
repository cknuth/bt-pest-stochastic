# This file contains useful equations for analyzing the bt pesticide system using SymPy
# see https://github.com/sympy/sympy
#
# to use on the python interpreter execute `from equations.six_type import *`

from sympy import *

# params
F, M_R, M_B = symbols('F M_R M_B', positive=true, real=true, integer=true)
s1, s2 = symbols('s1 s2', postive=true, real=true)

## EXPECTATION
# at time t
x1, x2, x3, y1, y2, y3 = symbols('x1 x2 x3 y1 y2 y3', positive=true, real=true, integer=true)

eg_0 = 2*(x1 + y1) + x2 + y2
N_0 = x1 + x2 + x3 + y1 + y2 + y3
NB_0 = x1 + x2 + x3
NR_0 = y1 + y2 + y3

# at time t + 0.25
x1_25 = (1 - M_B)*x1 + M_R*y1
x2_25 = (1 - M_B)*x2 + M_R*y2
x3_25 = (1 - M_B)*x3 + M_R*y3
y1_25 = (1 - M_R)*y1 + M_B*x1
y2_25 = (1 - M_R)*y2 + M_B*x2
y3_25 = (1 - M_R)*y3 + M_B*x3

eg_25 = 2*(x1_25 + y1_25) + x2_25 + y2_25
N_25 = x1_25 + x2_25 + x3_25 + y1_25 + y2_25 + y3_25
NB_25 = x1_25 + x2_25 + x3_25
NR_25 = y1_25 + y2_25 + y3_25

# at time t + 0.5
x1_5 = F/2*(x1_25**2/NB_25 + 2*x1_25*x2_25/NB_25 + x2_25**2/(2*NB_25))
x2_5 = F/2*(2*x1_25*x2_25/NB_25 + x2_25**2/(2*NB_25) + 2*x2_25*x3_25/NB_25)
x3_5 = F/2*(x3_25**2/NB_25 + 2*x3_25*x2_25/NB_25 + x2_25**2/(2*NB_25))
y1_5 = F/2*(y1_25**2/NR_25 + 2*y1_25*y2_25/NR_25 + y2_25**2/(2*NR_25))
y2_5 = F/2*(2*y1_25*y2_25/NR_25 + y2_25**2/(2*NR_25) + 2*y2_25*y3_25/NR_25)
y3_5 = F/2*(y3_25**2/NR_25 + 2*y3_25*y2_25/NR_25 + y2_25**2/(2*NR_25))

eg_5 = 2*(x1_5 + y1_5) + x2_5 + y2_5
N_5 = x1_5 + x2_5 + x3_5 + y1_5 + y2_5 + y3_5
NB_5 = x1_5 + x2_5 + x3_5
NR_5 = y1_5 + y2_5 + y3_5

# at time t + 0.75
x1_75 = s1*x1_5
x2_75 = s2*x2_5
x3_75 = x3_5
y1_75 = y1_5
y2_75 = y2_5
y3_75 = y3_5

eg_75 = 2*(x1_75 + y1_75) + x2_75 + y2_75
N_75 = x1_75 + x2_75 + x3_75 + y1_75 + y2_75 + y3_75
NB_75 = x1_75 + x2_75 + x3_75
NR_75 = y1_75 + y2_75 + y3_75

# at time t + 1
x1_1 = NB_0*x1_75/NB_75
x2_1 = NB_0*x2_75/NB_75
x3_1 = NB_0*x3_75/NB_75
y1_1 = NR_0*y1_75/NR_75
y2_1 = NR_0*y2_75/NR_75
y3_1 = NR_0*y3_75/NR_75

eg_1 = 2*(x1_1 + y1_1) + x2_1 + y2_1
N_1 = x1_1 + x2_1 + x3_1 + y1_1 + y2_1 + y3_1
NB_1 = x1_1 + x2_1 + x3_1
NR_1 = y1_1 + y2_1 + y3_1

# captured result of simplify(eg_1 - eg_0)

exp = ((2*s1*(2*(M_R*y1 - x1*(M_B - 1))*(M_R*y1 + 2*M_R*y2 - x1*(M_B - 1) - 2*x2*(M_B - 1)) + (M_R*y2 - x2*(M_B - 1))**2) + s2*(M_R*y2 - x2*(M_B - 1))*(4*M_R*y1 + M_R*y2 + 4*M_R*y3 - 4*x1*(M_B - 1) - x2*(M_B - 1) - 4*x3*(M_B - 1)))*(x1 + x2 + x3)*(2*(M_B*x1 - y1*(M_R - 1))*(M_B*x1 + 2*M_B*x2 - y1*(M_R - 1) - 2*y2*(M_R - 1)) + 2*(M_B*x2 - y2*(M_R - 1))**2 + (M_B*x2 - y2*(M_R - 1))*(4*M_B*x1 + M_B*x2 + 4*M_B*x3 - 4*y1*(M_R - 1) - y2*(M_R - 1) - 4*y3*(M_R - 1)) + 2*(M_B*x3 - y3*(M_R - 1))*(2*M_B*x2 + M_B*x3 - 2*y2*(M_R - 1) - y3*(M_R - 1))) + (y1 + y2 + y3)*(4*(M_B*x1 - y1*(M_R - 1))*(M_B*x1 + 2*M_B*x2 - y1*(M_R - 1) - 2*y2*(M_R - 1)) + 2*(M_B*x2 - y2*(M_R - 1))**2 + (M_B*x2 - y2*(M_R - 1))*(4*M_B*x1 + M_B*x2 + 4*M_B*x3 - 4*y1*(M_R - 1) - y2*(M_R - 1) - 4*y3*(M_R - 1)))*(s1*(2*(M_R*y1 - x1*(M_B - 1))*(M_R*y1 + 2*M_R*y2 - x1*(M_B - 1) - 2*x2*(M_B - 1)) + (M_R*y2 - x2*(M_B - 1))**2) + s2*(M_R*y2 - x2*(M_B - 1))*(4*M_R*y1 + M_R*y2 + 4*M_R*y3 - 4*x1*(M_B - 1) - x2*(M_B - 1) - 4*x3*(M_B - 1)) + (M_R*y2 - x2*(M_B - 1))**2 + 2*(M_R*y3 - x3*(M_B - 1))*(2*M_R*y2 + M_R*y3 - 2*x2*(M_B - 1) - x3*(M_B - 1))) - (2*x1 + x2 + 2*y1 + y2)*(s1*(2*(M_R*y1 - x1*(M_B - 1))*(M_R*y1 + 2*M_R*y2 - x1*(M_B - 1) - 2*x2*(M_B - 1)) + (M_R*y2 - x2*(M_B - 1))**2) + s2*(M_R*y2 - x2*(M_B - 1))*(4*M_R*y1 + M_R*y2 + 4*M_R*y3 - 4*x1*(M_B - 1) - x2*(M_B - 1) - 4*x3*(M_B - 1)) + (M_R*y2 - x2*(M_B - 1))**2 + 2*(M_R*y3 - x3*(M_B - 1))*(2*M_R*y2 + M_R*y3 - 2*x2*(M_B - 1) - x3*(M_B - 1)))*(2*(M_B*x1 - y1*(M_R - 1))*(M_B*x1 + 2*M_B*x2 - y1*(M_R - 1) - 2*y2*(M_R - 1)) + 2*(M_B*x2 - y2*(M_R - 1))**2 + (M_B*x2 - y2*(M_R - 1))*(4*M_B*x1 + M_B*x2 + 4*M_B*x3 - 4*y1*(M_R - 1) - y2*(M_R - 1) - 4*y3*(M_R - 1)) + 2*(M_B*x3 - y3*(M_R - 1))*(2*M_B*x2 + M_B*x3 - 2*y2*(M_R - 1) - y3*(M_R - 1))))/((s1*(2*(M_R*y1 - x1*(M_B - 1))*(M_R*y1 + 2*M_R*y2 - x1*(M_B - 1) - 2*x2*(M_B - 1)) + (M_R*y2 - x2*(M_B - 1))**2) + s2*(M_R*y2 - x2*(M_B - 1))*(4*M_R*y1 + M_R*y2 + 4*M_R*y3 - 4*x1*(M_B - 1) - x2*(M_B - 1) - 4*x3*(M_B - 1)) + (M_R*y2 - x2*(M_B - 1))**2 + 2*(M_R*y3 - x3*(M_B - 1))*(2*M_R*y2 + M_R*y3 - 2*x2*(M_B - 1) - x3*(M_B - 1)))*(2*(M_B*x1 - y1*(M_R - 1))*(M_B*x1 + 2*M_B*x2 - y1*(M_R - 1) - 2*y2*(M_R - 1)) + 2*(M_B*x2 - y2*(M_R - 1))**2 + (M_B*x2 - y2*(M_R - 1))*(4*M_B*x1 + M_B*x2 + 4*M_B*x3 - 4*y1*(M_R - 1) - y2*(M_R - 1) - 4*y3*(M_R - 1)) + 2*(M_B*x3 - y3*(M_R - 1))*(2*M_B*x2 + M_B*x3 - 2*y2*(M_R - 1) - y3*(M_R - 1))))
