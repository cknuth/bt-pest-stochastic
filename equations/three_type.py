
from sympy import *

# params
F = symbols('F', positive=true, real=true, integer=true)
s1, s2, s3 = symbols('s1 s2 s3', postive=true, real=true)

## EXPECTATION
# at time t
x1, x2, x3 = symbols('x1 x2 x3', positive=true, real=true, integer=true)

N = x1 + x2 + x3

x1_3 = F/2*(x1**2/N + 2*x1*x2/N + x2**2/(2*N))
x2_3 = F/2*(2*x1*x2/N + x2**2/(2*N) + 2*x2*x3/N)
x3_3 = F/2*(x3**2/N + 2*x3*x2/N + x2**2/(2*N))

N_3 = x1_3 + x2_3 + x3_3

w1, w2, w3 = symbols('w1 w2 w3', positive=true, real=true, integer=true)

x1_7 = w1 * s1
x2_7 = w2 * s2
x3_7 = w3 * s3

N_7 = x1_7 + x2_7 + x3_7
