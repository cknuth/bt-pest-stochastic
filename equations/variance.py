
import sympy as sp

def l(s):
    print sp.printing.latex(s).replace('left', 'big').replace('right', 'big')

F, x1, x2, x3, s1, s2, s3 = sp.symbols('F x1 x2 x3 s1 s2 s3', positive=True, real=True)
N = x1 + x2 + x3

p1 = x1 * x1 / N**2
p2 = x1 * x2 / N**2
p3 = x1 * x3 / N**2
p4 = x2 * x1 / N**2
p5 = x2 * x2 / N**2
p6 = x2 * x3 / N**2
p7 = x3 * x1 / N**2
p8 = x3 * x2 / N**2
p9 = x3 * x3 / N**2

# population values after mating/reproduction
w1 = F * N / 2 * (p1 + p2/2 + p4/2 + p5/4)
w2 = F * N / 2 * (p2/2 + p3 + p4/2 + p5/2 + p6/2 + p7 + p8/2)
w3 = F * N / 2 * (p5/4 + p6/2 + p8/2 + p9)

y1 = s1 * w1
y2 = s2 * w2
y3 = s3 * w3

N_2 = y1 + y2 + y3

z1 = (N * y1) / N_2
z2 = (N * y2) / N_2
z3 = (N * y3) / N_2

g = 2*x3 + x2
f = (2*x3 + x2) / (2*N)

d1 = (4*w3*s3*(1-s3+w3*s3) - 8*w3**2*s3 + 4*w3**2 + w2*s2*(1-s2+w2*s2) - 2*w2**2*s2 + w2**2 + 4*w2*s2*w3*s3 - 4*w3*s3*w2 - 4*w2*s2*w3 + 4*w2*w3) / (4*N**2)
# d1 = sp.simplify(d1)

d2 = (2*(s3 - 1)*w3 + (s2 - 1)*w2)

f0 = (2*x3 + x2) / (2*N)

e = (2*z3 + z2) / (2*z1 + 2*z2 + 2*z3) - f0
# e = sp.simplify(e)

final_e = ( (s2 - s1)*f0 + (s3 - 3*s2 + 2*s1)*f0**2 - (s3 - 2*s2 + s1)*f0**3 ) / ( s1 + 2*(s2 - s1)*f0 + (s3 - 2*s2 + s1)*f0**2 )

e2 = d1 - d2**2 / (4*N**2) + e**2
# v = sp.simplify(v)
e2 = (
        -F**2*((s2 - 1)*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3) + (s3 - 1)*(x2**2 + 4*x2*x3 + 4*x3**2))**2
        *(s1*(4*x1**2 + 4*x1*x2 + x2**2) + 2*s2*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3) + s3*(x2**2 + 4*x2*x3 + 4*x3**2))**2
        + F*(
            F*(
                s2*s3*(x2**2 + 4*x2*x3 + 4*x3**2)*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)
                - s2*(x2**2 + 4*x2*x3 + 4*x3**2)*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)
                - 2*s2*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)**2
                - 2*s3*(x2**2 + 4*x2*x3 + 4*x3**2)**2
                - s3*(x2**2 + 4*x2*x3 + 4*x3**2)*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)
                + (x2**2 + 4*x2*x3 + 4*x3**2)**2
                + (x2**2 + 4*x2*x3 + 4*x3**2)*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)
                + (2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)**2
            )
            + s2*(
                F*s2*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)
                + 4*(-s2 + 1)*(x1 + x2 + x3)
            )*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3)
            + s3*(
                F*s3*(x2**2 + 4*x2*x3 + 4*x3**2)
                + 8*(-s3 + 1)*(x1 + x2 + x3)
            )*(x2**2 + 4*x2*x3 + 4*x3**2)
        )*(s1*(4*x1**2 + 4*x1*x2 + x2**2) + 2*s2*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3) + s3*(x2**2 + 4*x2*x3 + 4*x3**2))**2
        + 16*(
            (x2 + 2*x3)*
            (s1*(4*x1**2 + 4*x1*x2 + x2**2) + 2*s2*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3) + s3*(x2**2 + 4*x2*x3 + 4*x3**2))
            - 2*(s2*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3) + s3*(x2**2 + 4*x2*x3 + 4*x3**2))*(x1 + x2 + x3)
        )**2
        * (x1 + x2 + x3)**2)\
    / (
        64*(x1 + x2 + x3)**4
        *(s1*(4*x1**2 + 4*x1*x2 + x2**2) + 2*s2*(2*x1*x2 + 4*x1*x3 + x2**2 + 2*x2*x3) + s3*(x2**2 + 4*x2*x3 + 4*x3**2))**2
    )

final_e2 = F / (4*N) * ( s2*(1 - s2)*f0*(1-f0) + 2*s3*(1 - s3)*f0**2 ) + e**2

v2 = d1 - d2**2 / (4*N**2)
final_v2 = F / (4*N) * ( s2*(1 - s2)*f0*(1-f0) + 2*s3*(1 - s3)*f0**2 )

# expectation of the squared X_3 first step increment
e_s3 = F**2 * N**2 / 4 * (p5/16 + p6/4 + p8/4 + p9) + x3**2 - x3*N*F*(p5/4 + p6 + p9)
# expectation of the squared X_2 first step increment
e_s2 = F**2 * N**2 / 4 * (p2/2 + 2*p3 + p5/4 + p6/2) + x2**2 - x3*N*F*(p2 + 2*p3 + p5/2 + p6)
# expectation of the cross X_3, X_2 first step increment
e_s23 = N**2 * F**2 / 4 * (p5/8 + p6/2) + x2*x3 - x3*N/2*(p2 + 2*p3 + p5/2 + p6) - x2*N/2*(p5/4 + p6 + p9)
# expectation of delta g first step
e_dg1 = F*N*(p5/4 + p6 + p9) - 2*x3 + F*N/2*(p2 + 2*p3 + p5/2 + p6) - x2
